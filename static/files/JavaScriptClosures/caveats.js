function caveat1() {
	//this will create array with three closures in it
	var myFncts1 = getFunctionSet(1);
	var myFncts2 = getFunctionSet(10);
	//we'll examine array twice
	for(var i = 0; i < 2; i++) {
		//and we'll call each closure in that array
		for(var j = 0; j < myFncts1.length; j++) {
			//we could expect displaying 1, 2, 4, 4, 5, 10
			myFncts1[j]();
		}
	}
	//now again for the second array
	for(var k = 0; k < 2; k++) {
		for(var l = 0; l < myFncts2.length; l++) {
			//we could expect displaying 10, 11, 22, 22, 23, 46
			myFncts2[l]();
		}
	}
	//as you can see, both arrays keeps their own stack
	alert(
		"Final value of first set is " + myFncts1[3]() + "\n" +
		"Final value of second set is " + myFncts2[3]()
	);
}

function getFunctionSet(startingValue) {
	var number = startingValue;
	return [
		function() {alert(number)},
		function() {number++; alert(number)},
		function() {number=number*2; alert(number)},
		function() {return number},
	];
}

function caveat2() {
	var myFnct = getCaveat2function();
	//if you think value 1 will be displayed,
	//you're terribly wrong - neither 1 or error will occur
	myFnct();
	//when we need to fix variable values we need to use objects
	getCaveat2functionKeepingItsOriginalValue().showNumber();
}

function getCaveat2function() {
	var number = 1;
	var myFnct = function() {alert(number + anotherNumber)};
	number ++;
	var anotherNumber = 50;
	return myFnct;
}

function getCaveat2functionKeepingItsOriginalValue() {
	var number = 1;
	//for fixing values we need to create objects
	var MyFnct = function(number) {
		//this will copy number value at the time
		//instance of this object is created
		var myNumber = number;
		//by this declaration we'll create new method
		//of this object that simply displays inner value
		this.showNumber = function() {
			alert(myNumber)
		}
	};
	//in this moment variables are copied
	var result = new MyFnct(number);
	//this won't affect result inner value
	number++;
	return result;
}

function caveat3() {
	var data = ["Janek","Pepa","Luca"];
	var myFncts1 = getLoopFunctionSet(data);
	for(var i = 0; i < myFncts1.length; i++) {
		//do you think we'll see Closure #Janek, Closure #Pepa, Closure #Luca ?
		//nope! we'll see Closure #undefined, Closure #undefined, Closure #undefined !
		//why? because variable i value at the end of method getLoopFunctionSet is 3
		myFncts1[i]();
	}
	//but we can solve it with objects pattern
	var myFncts2 = getLoopFunctionSetSolution(data);
	for(var j = 0; j < myFncts2.length; j++) {
		myFncts2[j].showIt();
	}
	//but we can solve it with extended function pattern
	var myFncts3 = getLoopFunctionSetSolution(data);
	for(var k = 0; k < myFncts3.length; k++) {
		myFncts3[k].showIt();
	}
}

//naive method
function getLoopFunctionSet(sourceList) {
	var result = new Array(sourceList.length);
	for(var i = 0; i < sourceList.length; i++) {
		result[i] = function() {alert("Closure #" + sourceList[i])};
	}
	return result;
}

//data transfer object pattern
function getLoopFunctionSetSolution(sourceList) {
	var result = new Array(sourceList.length);
	for(var i = 0; i < sourceList.length; i++) {
		var Dto = function() {
			var innerValue = sourceList[i];
			this.showIt = function() {alert("Closure #" + innerValue)};
		};
		result[i] = new Dto();
	}
	return result;
}

//function transfer pattern
function getLoopFunctionSetAnotherSolution(sourceList) {
	var result = new Array(sourceList.length);
	for(var i = 0; i < sourceList.length; i++) {
		//calling extendedFunction will enforce javascript to copy value of variable
		//on curent position in array
		result[i] = extendedFunction(sourceList[i]);
	}
	return result;
}

function extendedFunction(name) {
	return function() {alert("Closure #" + name)};
}