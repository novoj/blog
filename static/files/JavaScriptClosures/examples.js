function example1() {
   //this shows no allert
   var myFnct = function() { alert("Hello World!") };
   //this shows function as string - no execution will happen
   alert(myFnct.toString());
   //there we'll execute it
   myFnct();
}

function example2() {
   //we'll fetch a function and execute it on next line
   var myFnct = getSomeFunction();
   myFnct("Father Fourah");
   //we can do it even in shorter way - looks quite ridiculous - doesn't it?
   getSomeFunction()("Reader");
}

function getSomeFunction() {
   return function(name) {alert("Hello " + name)};
}

function example3() {
   var names = ["Jan", "Petr", "Milan"];
   var myFnct = function(name) {alert(name)};
   forEachExecute(names, myFnct);
   //or the same in more compressed way
   forEachExecute([1,2,3], function(nmb) {alert(nmb)});
}

function forEachExecute(data, callback) {
   for(var i = 0; i < data.length; i++) {
      callback(data[i]);
   }
}

function example4() {
	//this is equivalent to anonymous function declaration
	var myFnct = someFunction;
	//this proves that function isn't executed until next line 
	alert("Before function execution");
	myFnct();
	//the same is valid for method with parameters
	//we can deliver parameters only when we call method not before
	var myFnct2 = someFunctionWithParam;
	alert("Before second function execution");
	myFnct2("Jan");
	//this won't work as it executes function immediately
	//and assigns null value to our variable
	var myFnct3 = someFunctionWithParam("Petr");
	alert("As you can see this is displayed after execution of third function, value "
			+ myFnct3 + " is assigned to variable, not a function itself.");
}

function someFunction() {
	alert("Hello world!");
}

function someFunctionWithParam(name) {
	alert("Hello " + name + "!");
}

function example5() {
	var myFnct = getFunctionExample5("Just kidding.");
	//can you see? getFunctionExample5 method scope is closed now
	//but we can still access local variable name of that scope via closure
	//in example we are mixing even method parameters
	myFnct("No, no - I am serious.");
}

function getFunctionExample5(suffix) {
	var name = "Father Fourah";
	return function(postSuffix) {alert(name + " rulez!\n" + suffix + "\n" + postSuffix)};
}

function example6() {
	var myFnct = getFunctionExample6();
	//this doesn't work no closure was created
	//we have acquired only method pointer
	myFnct();
}

function getFunctionExample6() {
	var name = "Father Fourah";
	return example6function;
}

function example6function() {
	alert(name + " See? Name is not know in this case - this doesn't create closure!");
}

function example7() {
	var myFnct = getFunctionExample7();
	//but in case of inner methods closures are created
	myFnct();
}

function getFunctionExample7() {
	var name = "Father Fourah";

	function example7function() {
		alert(name + " can use even inner functions - these are closures!")
	}
	
	return example7function;
}

function example8() {
	var myFnct = getFunctionExample8();
	//but closures keep reference to whole stack tree
	//not only to stack of method closure is created in
	myFnct()();
}

function getFunctionExample8() {
	var name = "Father Fourah";
	return function() {
		var suffix = "goes insane!"
		return function() {
			alert(name + " " + suffix);
		}
	}
}