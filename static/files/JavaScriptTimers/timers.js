var refreshInterval = 1000;
var originalTime;
var ticks = 0;
var slowTicks = 0;

$(document).ready(function() {    
	originalTime = new Date();
	$("#originalTime").text(originalTime.toString());
	setInterval(logTime, refreshInterval);
	setInterval(slowFunction, refreshInterval / 3);
});

function logTime() {
    var now = new Date();
	var html = now.valueOf() + " -&gt; " + now + "<br>";
	ticks = ticks + 1;
	$("#ticks").text(ticks);
	$("#diff").text(now.valueOf() - (originalTime.valueOf() + (ticks * refreshInterval)));
	$("#results").prepend(html);	
}

function slowFunction() {
   for (var i = 0; i < 3; i ++) {
      slowTicks = $("#originalTime").text().length + 1;
   }
}

function doSomethingDemanding() {
	if ($("body").css("opacity") != 1) {
		$("body").animate({ opacity: 1}, 2000);
	} else {
		$("body").animate({ opacity: 0.2}, 2000);
	}
}