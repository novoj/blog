# O autorovi

Jmenuji se Jan Novotný a jsem vývojářem, který spojil svou velkou vášeň s prací, která ho živí.
Blog jsem začal psát v roce 2006 jako součást svého vzdělávání v oboru. Psaní blogu mě umožnilo se
zúčastnit prvního ročníku ne-konference [jOpenSpace](https://www.jopenspace.cz), kterou jsem po několika
letech organizačně od Michala Šrajera převzal doposud pomáhám organizovat. Blogování časem ustoupilo
nahrávání podcastu [Kafemlejnek TV](https://kafemlejnek.tv), které natáčím spolu s kamarádem Petrem
Ferschmannem, se kterým jsem se také setkal na jOpenSpace ne-konferenci.

Mým hlavním oborem je backendová část aplikací a dlouhá léta jsem věrný jazyku Java. Pro mého
zaměstnavatele [FG Forrest](https://www.fg.cz) jsem vedle komerčních projektů napsal celou řadu nástrojů,
které společnost využívá dosud. Jsem fanouškem Linuxu a open-source software a ve FG se snažím smysluplné
části otevírat k veřejnému použití pod skupinou [one.edee.oss](https://mvnrepository.com/artifact/one.edee.oss).

V současné době pracuji na platformě [Edee.one](https://www.edee.one) a především na její e-commercové
části, která pohání desítky e-shopů s obratem ve stovkách milionů korun. V současné době jsem součástí
týmu pro výzkum a vývoj, který má přinést novou generaci této platformy se zaměřením na bezprecedentní
výkon a jednoduchost použití.