---
status: draft
published: false
title: Histogram
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
date: '2019-09-14 17:19:57 +0200'
date_gmt: '2019-09-14 17:19:57 +0200'
categories:
- Programming
tags: [E-commerce]
comments: []
---
First tests with in memory index were using [CQ Engine](https://github.com/npgall/cqengine) that promised to deliver 
unprecedented performance when querying Java collections. Unfortunately this wasn't true for queries that requested
values greater than (or equals), lesser than (or equals), between, overlap and outer boundary. On the contrary in these
types of lookup **CQ Engine** (tested on v 3.4) was slower than simple iteration through entire collection with 
predicate application.

This led us to use custom data structures that would handle this types of queries really fast. One of such structures
is **Histogram**. Histogram is two dimensional array where first dimension contains comparable values in ascending order
and the second dimension ids of all records that posses this value (also in ascending order in our case). Simple example
of such histogram might look like this:

| Resolution | Records                   |
|------------|---------------------------|
| 144p       | 1, 5,12,18,32             |
| 240p       | 4, 5, 6, 9                |
| 360p       | 13                        |
| 720p       | 2, 4, 5,11,16,18,22       |
| 1080p      | 3, 6, 8,14,18,19,22,24,29 |

This data structure allows to perform fast queries of these types:

### Lesser than / greater than (or equals)

By using [binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm) (that has O log N complexity) on first 
dimension we lookup the threshold value. Then we can take all pointer to second dimension that before / after this position
and execute [OR matrix operation](2019-09-14-logical-operations-over-matrices-of-sorted-numbers.md#logical-disjunction-or) 
on them.

Let's show some example using data in above displayed table. When all records with resolution `360p` and less is requested
we look up for position of resolution `360p` in first dimension of the histogram. It's on the position 2 (indexing starts with zero).
All products with resolution `360p` and lesser will lay on positions:
 
| Resolution | Records                   |
|------------|---------------------------|
| 144p       | 1, 5,12,18,32             |
| 240p       | 4, 5, 6, 9                |
| 360p       | 13                        |

When we perform OR on these ids we'll get the result: 1, 4, 5, 6, 9, 12, 13, 18, 32

### Between (inclusive / exclusive)

For this query two binary searches are required. One for each boundary threshold. When we have both thresholds we compute
[OR matrix operation](2019-09-14-logical-operations-over-matrices-of-sorted-numbers.md#logical-disjunction-or) of all 
records on secondary dimension located on positions between those two threshold positions on first dimension. Looks
complex but it's really simple in reality.

Let's use another example to demonstrate this case. We need to select all items with resolution between `240p` and `720p`
including those resolutions. Binary searches will return position 1 for `240` and 3 for `720p` (indexing starts with zero).
This means we have to bring together all following matrices:  

| Resolution | Records                   |
|------------|---------------------------|
| 240p       | 4, 5, 6, 9                |
| 360p       | 13                        |
| 720p       | 2, 4, 5,11,16,18,22       |

Performing OR operation on them will bring us result: 2, 4, 5, 6, 9, 11, 16, 18, 22

Exclusive search is identical with the difference that matrices exactly on position of the thresholds are not part of the
OR operation.

### Outside (inclusive / exclusive)

Histogram allows us to easily compute even all records that are NOT between certain boundaries. This computation is only
inverse operation of the [between](#between-inclusive--exclusive) operation. When we lookup for left and right threshold
we collect all matrices on dimensions zero to left boundary and right boundary to end position and perform [OR matrix operation](2019-09-14-logical-operations-over-matrices-of-sorted-numbers.md#logical-disjunction-or)
on them.

Let's see how computation would look like for previous example - we want to get all records which resolution is not between
`240p` and `720p`. Result would be this: 

| Resolution | Records                   |
|------------|---------------------------|
| 144p       | 1, 5,12,18,32             |
| 1080p      | 3, 6, 8,14,18,19,22,24,29 |

Performing OR operation on them will bring us result: 1, 3, 5, 6, 8, 12, 14, 18, 19, 22, 24, 29, 32

### Laziness

Result of the histogram query can be computed at once but since the [matrix operations](2019-09-14-logical-operations-over-matrices-of-sorted-numbers.md)
are implemented as iterators, all operations listed for histogram behave lazy as well. That means that the only price
paid immediately is the binary search for thresholds and collection of all pointers on matrices in secondary dimension.
Computation cost is paid gradually as we iterate through the iterator and may not be paid completely if we don't require
all the results.

## Performance

Performance is the main reason why we use this data structure so examine it's behaviour on rather big data. Let's generate
1 milion of record ids that will be randomly assigned to 10 thousands thresholds in histogram. Task would be to return
list of record ids between certain thresholds. I will compare these 3 scenarios:

1. plain two dimensional Java array access, first position will contain threshold, second record id
2. using CQ engine library to perform lookup for us
3. using above described Histogram data structure and [matrix binary operations](2019-09-14-logical-operations-over-matrices-of-sorted-numbers.md)

Because data are randomly generated I must invoke all three approaches on the same data set and compare relative difference
that can be then approximated to some form of performance gain/loss against some base line. On each iteration every approach
has to have it's warming phase so that JIT can inline all hot functions.