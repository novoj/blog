---
status: publish
published: true
title: Kafemlejnek.TV 42 – NETIO, chytré zásuvky pro průmysl (soutěž!)
aliases:
    - /2019/03/19/dil-42-netio-chytre-zasuvky-pro-prumysl/
date: '2019-03-19 21:02:00 +0100'
categories:
- Podcast
tags:
- kafemlejnektv
- hardware
comments: []
---
Rozhovor s Honzou Řehákem v NETIO Products byl poslední před vánocemi. Z výše uvedené věty je krásně vidět, jak dlouho nám trvá takový díl pro vás připravit (pravda měli jsme ještě něco předtočené ve frontě).

NETIO Products nás pozvali do svého sídla – rodinného domku na jihu Prahy. Jedná se o malý startup, který vyrábí a prodává chytré zásuvky po celé Evropě. Chytré zásuvky zvládají přesné měření protékajícího proudu, spotřeby a vzdálené řízení spínání. Zásuvky můžete programovat ve skriptovacím jazyce Lua, posílat jim XML, JSON, můžete jim zavolat přes SIP, nebo pingnout přes HTTP. Spínání můžete naschedulovat, jednoduše zajistíte autorestart neposlušných strojů na druhé straně zásuvky.

Zásuvky jsou navíc vyráběné z kvalitních komponent a autoři si dali dost práce s tím, aby mohli říct, že se jedná o zařízení vhodné pro průmysl. Například při aktualizaci firmware vám nevypadne na druhé straně proud. Stejně tak minimalizují náběhové zatížení. Mají za sebou i řadu certifikací, o kterých se v díle také pobavíme. Připravili jsme pro vás i prohlídku různých standardů elektrických zásuvek, které po Evropě můžete najít.

Nevím, jestli je to tou pizzou, kterou jsme dostali před natáčením nebo blížícími se vánoci, ale celý rozhovor se nese ve velmi veselém duchu a celé to zakončil Ferš, když při závěrečném natáčení “úvodu” odpojil testovací skript od tišťáku svým zvědavým pařátem.

Přes to všechno naši přátelé z NETIO Product vyhlásili SOUTĚŽ a hodlají se s vámi podělit o 3 jejich chytré zásuvky. Pravidla soutěže jsou uvedena zde:

https://www.netio-products.com/cs/novinky/kafemlejnek-tv-chytre-zasuvky-pro-prumysl

Cílem je naprogramovat nejzajímavější a nejužitečnější Lua skript pro ovládání zásuvky a vítěze soutěžeme vybereme společnými silami s Honzou Řehákem a zveřejníme je v některém z příštích Kafemlejnků.

### Kompletní obsah: https://kafemlejnek.tv/dil-42-netio-chytre-zasuvky-pro-prumysl-soutez/