---
status: publish
published: true
title: Forresti publikují Open source software
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
date: '2020-09-11 16:30:00 +0200'
date_gmt: '2020-09-11 16:30:00 +0200'
categories:
- Programming
tags: [Open-Source]
comments: []
---
Stejně jako většina firem v našem průmyslu i my jsme založili náš byznys na otevřeném softwaru. *Jsme trpaslíci, kteří 
stojí na ramenech obrů – nanos gigantum humeris insidentes.* Celé roky využíváme software vývojářů, kteří nás svými 
znalostmi převyšují a kteří svoji práci poskytli zdarma ku prospěchu všech.

Mnohokrát jsme si pokládali otázku, jak tento dar oplatit. Jednou z možností bylo přispívat do některých z OSS nadací, 
ale usoudili jsme, že peníze lépe pomohou jinde. Jako vývojáři jsme tíhnuli spíše k tomu, vracet ušetřenou práci darovanou 
prací, a proto jsme se snažili publikovat, bavit a vzdělávat, spojovat či přispívat opravami. Stále jsme diskutovali 
o tom, že bychom mohli část naší práce open-sourcovat, ale měli jsme obavy, že:

- naše práce nedosahuje potřebných standardů
- nebudeme mít čas na údržbu zveřejněných knihoven a případnou podporu
- ztratíme výhodu “security through obscurity”
- naše angličtina má mezery a budeme na mezinárodním poli pro smích

Od doby, kdy jsme toto naposledy řešili, uplynul nějaký čas a řada věcí u nás i ve vnímání OSS se změnila. Jsme starší
a víme, že práce nikdy nemůže být dokonalá, že “perfect is the enemy of good” a vždy je tu riziko, že pro někoho nebude 
naše práce dost dobrá, ale jinému naopak vytrhne trn z paty. Získali jsme výzkumný grant a můžeme si dovolit více času 
věnovat na jiné aktivity než dosavadní každodenní dodávání hodnoty našim klientům. Je možné, že i tak podceňujeme 
množství času, které může open source vyžadovat, ale pokud budeme brát open source jako dar, a ne jako závazek, bude se 
nám to zvládat lépe.

Za léta jsme si vyvinuli několik nástrojů, které nám věrně slouží a jsou otestované dlouholetou praxí. Na nich bychom 
si chtěli vyzkoušet, jaké je to fungovat ve světě OSS, prošlápnout si postupy a zjistit pozitiva a negativa. Příští rok 
bychom pak chtěli přijít s něčím ambicióznějším, ale o tom zase někdy příště.

Nyní tedy blíže ke dvěma knihovnám, které jsme se rozhodli zveřejnit pod svobodnou MIT licencí.

## Knihovna PMPTT

První ze zveřejňovaných knihoven je jednoduchý nástroj pro výpočet hranic pro algoritmus Modified Preorder Tree Traversal. 
Tento přístup ke stromovým strukturám umožňuje zjednodušit vyhodnocování nadřízených a podřízených položek na porovnání 
dvou čísel. Na toto téma má jeden z Forrestů přednášku na letošní konferenci JavaDays. Ačkoliv je to algoritmus poměrně 
starý, má jednu klíčovou nevýhodu, a tou jsou velké náklady na zápis nové položky, nebo přesuny ve struktuře. Tyto operace 
typicky představují přepočítání velké části stromu, a pokud jsou tyto hranice z důvodu větší výkonnosti SQL dotazů uložené 
ještě na dalším místě, je tato nevýhoda dále násobená.

Z toho důvodu jsme přišli s drobnou mutací tohoto algoritmu (nazvanou Preallocated Modified Preorder Tree Traversal), 
která při akceptaci určitých zjednodušení umožňuje definovat rovnici determinující hranice pro každý uzel stromu tak, 
aby zápisové operace, jako je přidání, odebrání nebo přesun uzlu v rámci stejného nadřízeného uzlu, negenerovaly žádné 
požadavky na přepočet hranic.

Zjednodušením, na které je nutné přistoupit, je dopředu definovat maximální počet úrovní zanoření ve stromu a maximální 
počet dětí jednoho uzlu. Zároveň je nutné se kombinatoricky vejít do datového typu long, což představuje cca 10 úrovní 
stromu do hloubky při 55 položkách v jednom uzlu.

Knihovna velmi zjednodušuje i operace přesunu uzlů mezi různými úrovněmi stromu (včetně podřízených uzlů), byť tyto 
operace zůstávají stejně zápisově náročné jako v původním algoritmu.

Knihovnu zveřejňujeme na:

- [GitHub](https://github.com/FgForrest/PMPTT)
- [Maven Repository Central](https://mvnrepository.com/artifact/one.edee.oss/pmptt)

O detailech této knihovny vyjde jeden z následujících článků. Jako ochutnávka by Vám mohla posloužit prezentace našeho 
kolegy z JavaDays, případně její videozáznam, pokud se konference zúčastníte.

## Knihovna Darwin

Darwin je jednou z našich nejstarších knihoven, kterou dosud masivně používáme (její počátky se datují do roku 2007), 
a umožňuje jednoduchou evoluci datového modelu pro vaši aplikaci. Darwin spouští migrační SQL skripty v konkrétním 
dialektu podle jasně daných pravidel. Vznikl pravděpodobně mnohem dříve než open-sourcové varianty Liquibase nebo FlywayDB. 
Je sice podstatně jednodušší, ale představuje pro nás nástroj, který je ve své jednoduchosti univerzální a spolehlivý.

Knihovna umožňuje na classpath udržovat SQL skripty umožňující založit tabulky, které vaše aplikace potřebuje, a to 
nejen pro jednu konkrétní databázi, ale pro několik různých databází najednou (tj. paralelně můžete pro své univerzální 
knihovny udržovat jak MySQL, tak třeba i Oracle schéma). To, které z nich aplikuje, si Darwin rozhodne po nastartování 
aplikace a zjištění typu databáze z objektu DataSource.

Když dále rozvíjíte svoji aplikaci, dojdete brzy k tomu, že stávající struktura tabulek vám nevyhovuje a je třeba ji 
rozšířit či změnit. K tomu vám stačí do Darwinovy složky uložit tzv. patch soubory, obsahující v názvu číslo verze 
aplikace, pro které je tato struktura potřeba. Při dalším startu aplikace si Darwin porovná aktuální verzi vaší aplikace 
(tj. tu, která právě nastartovala) s verzí, kterou naposledy aplikoval na databázové schéma. Pokud je verze aplikace 
novější, najde všechny patche mezi těmito dvěma verzemi a postupně je na schéma aplikuje.

Darwin si zároveň dává velký pozor na transakčnost změn a i pokud databáze neumožňuje zakrytí změn transakcí (tj. nedokáže
správně odrolovat DDL SQL příkazy), pamatuje si, které SQL příkazy se mu v rámci patche povedlo skutečně provést a které 
nikoliv. Díky tomu umí navázat svou činnost i uprostřed částečně aplikovaného patche. Toto je šikovné především ve fázi 
vývoje.

Knihovna počítá s modulárním přístupem, takže může být instanciovaná v aplikaci třeba 20x pro různé sdílené knihovny.

Darwin si dává velký pozor i na dohledatelnost všeho, co s vaší databází provedl. Všechny příkazy, které na váš pokyn 
provedl, jsou zalogované v jeho tabulkách se všemi potřebnými informacemi jako je datum objevení patche v aplikaci, 
datum aplikace patche do db, délka trvání jednotlivých SQL příkazů, případná výjimka, která při aplikaci SQL příkazu 
z databáze vypadla a mnoho dalšího.

Darwin také obsahuje nástroj Locker umožňující synchronizovat úlohy v rámci clusteru v případě, že existuje sdílená 
databáze. Tuto Locker třídu Darwin používá k tomu, aby při souběžném startu stejné aplikace na více nodech v clusteru 
aplikoval SQL patche pouze na jednom z nodů a ostatní instance Darwina poslušně na dokončení migrace počkaly. Nástroj 
ovšem můžete použít i pro jiné účely – například my jej používáme poměrně aktivně pro kontrolu spouštění asynchronních 
úloh nejen v rámci clusteru, ale i na jedné instanci JVM.

Další detaily budou popsány v angličtině na domovské stránce tohoto nástroje.

Knihovnu zveřejňujeme na:

- [GitHub](https://github.com/FgForrest/Darwin)
- [Maven Repository Central](https://mvnrepository.com/artifact/one.edee.oss/darwin)

Článek o této knihovně už vám neslíbíme. Nepředpokládáme, že by vyvolala větší zájem, jelikož na trhu již existují 
mnohem pokročilejší a rozšířenější nástroje s velkou komunitou. Pro nás je však takovým malým odrazovým můstkem pro 
open-sourcování našich dalších knihoven, které Darwina historicky používají, a díky naší spokojenosti s ním vlastně 
nechceme ta složitější řešení používat. Možná kdybychom tenkrát v roce 2007 šli rovnou do OSS, byla by teď situace jiná :)

Držte nám palce…

… open source děláme vlastně poprvé a nevíme tak úplně, do čeho jdeme. Možná to nebude znamenat nic, možná nás to posune 
o notný kus dál. Nevíme, dokud to nezkusíme.