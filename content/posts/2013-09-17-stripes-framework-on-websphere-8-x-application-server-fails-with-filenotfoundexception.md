---
status: publish
published: true
title: Stripes Framework on WebSphere 8.x application server fails with FileNotFoundException
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
excerpt: "<img class=\"alignleft  wp-image-2766\" alt=\"stripes_framework\" src=\"/binary/2013/09/stripes_framework.png\"
  width=\"121\" height=\"121\" />\r\n\r\nIf you are struggling with running <a href=\"stripesframework.org\"
  target=\"_blank\">Stripes framework</a> based application on IBM WebSphere application
  server 8.x I may help you. You probably end up with getting HTTP 403 error pages
  on every request. If you persuade your app server to give you some reasonable logging
  data (which is not easy in the world of Java application servers :/ ) you probably
  see FileNotFoundException logged during the request.\r\n\r\nHere comes the explanation
  and a solution for you ...\r\n\r\n<em> (bug is also documented in issue <a href=\"http://www.stripesframework.org/jira/browse/STS-905\"
  target=\"_blank\">STS-905</a> and will be hopefully merged into the next version
  of Stripes framework)</em>\r\n\r\n"
wordpress_id: 2765
wordpress_url: http://blog.novoj.net/?p=2765
aliases:
    - /?p=2765
date: '2013-09-17 11:52:02 +0200'
date_gmt: '2013-09-17 10:52:02 +0200'
categories:
- Programování
- Stripes
tags: []
comments: []
---
<p><img class="alignleft  wp-image-2766" alt="stripes_framework" src="/binary/2013/09/stripes_framework.png" width="121" height="121" /></p>
<p>If you are struggling with running <a href="stripesframework.org" target="_blank">Stripes framework</a> based application on IBM WebSphere application server 8.x I may help you. You probably end up with getting HTTP 403 error pages on every request. If you persuade your app server to give you some reasonable logging data (which is not easy in the world of Java application servers :/ ) you probably see FileNotFoundException logged during the request.</p>
<p>Here comes the explanation and a solution for you ...</p>
<p><em> (bug is also documented in issue <a href="http://www.stripesframework.org/jira/browse/STS-905" target="_blank">STS-905</a> and will be hopefully merged into the next version of Stripes framework)</em></p>
<p><a id="more"></a><a id="more-2765"></a>Stripes use 2 servlet filters to handle all requests - StripesFilter and DynamicMappingFilter. First one prepares request for later processing and cleans up afterwards and is not important for us in this case. The problem lies in DynamicMappingFilter in following statement:</p>


``` java

// Catch FileNotFoundException, which some containers (e.g. GlassFish) throw instead of setting SC_NOT_FOUND
boolean fileNotFoundExceptionThrown = false;
try {
   chain.doFilter(request, wrapper);
} catch (FileNotFoundException exc) {
   fileNotFoundExceptionThrown = true;
}
```

<p>DynamicMappingFilter delegates request processing through filter chain and when application server says that there is no resource there it gets into the way and tries to find appropriate ActionBean for it to handle request. In version 1.5.1 of the Stripes framework it only wrapped HTTP response object to intercept setting status code SC_NOT_FOUND for detecting such situation but in version 1.5.7 there is also try ... catch clausule for FileNotFoundException that is thrown instead on some application servers.</p>
<p>And there is the catch - WebSphere 8.x wraps FileNotFoundException into the ServletException and thus it is not caught by this code. Until patch is included in the Stripes framework core you have to duplicate the filter class with application of this patch:</p>


``` java

try {
    chain.doFilter(request, wrapper);
} catch (FileNotFoundException exc) {
    fileNotFoundExceptionThrown = true;
} catch (ServletException ex) {
    if (ex.getCause() instanceof FileNotFoundException) {
        fileNotFoundExceptionThrown = true;
    } else {
        throw ex;
    }
}
```

<p>And there is another tweak you have to make - in the very same class you have to modify inner class <em>ErrorTrappingResponseWrapper</em> by overriding two more methods:</p>


``` java

@Override
public void setStatus(int sc) {
   this.errorCode = sc;
}
@Override
public void setStatus(int sc, String sm) {
   this.errorCode = sc;
   this.errorMessage = sm;
}
```

<p><a href="https://gist.github.com/novoj/6593787" target="_blank">Click here</a> to download full class.</p>
<p>Also you have to alter your web.xml configuration to map patched filter:</p>


``` xml

<filter>
   <filter-name>DynamicMappingFilter</filter-name>
   <filter-class>net.sourceforge.stripes.controller.HackedDynamicMappingFilter</filter-class>
</filter>
....
<filter-mapping>
   <filter-name>DynamicMappingFilter</filter-name>
   <url-pattern>/*</url-pattern>
   <dispatcher>REQUEST</dispatcher>
   <dispatcher>FORWARD</dispatcher>
   <dispatcher>INCLUDE</dispatcher>
</filter-mapping>

```

<p>Happy coding!</p>
