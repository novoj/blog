---
status: publish
published: true
title: Spring profiles a použití v testech
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
excerpt: "<img class=\"alignleft  wp-image-2564\" title=\"Spring 3.1\" src=\"/binary/2013/05/Logo_Spring_258x151.png\"
  alt=\"\" width=\"181\" height=\"106\" />Po 3 letech dělám větší refaktoring na našem
  direct mailingovém modulu a jako první jsem se rozhodl povýšit verze knihoven a
  zrefaktorovat JUnit testy, které jsou tam ještě psané ve stylu JDK 1.4.\r\n\r\nV
  souvislosti s tím jsem samozřejmě přepracoval formu testů z dědičné hierarchie na
  anotace, které byly představeny poprvé ve <a href=\"http://static.springsource.org/spring/docs/3.2.x/spring-framework-reference/html/testing.html\"
  target=\"_blank\">Spring 3.X</a> (pokud se nepletu). A tu jsem zjistil, že mám drobný
  problém - v původní verzi mého kódu jsem využíval dynamické kompozice Springového
  kontextu k tomu, abych stejné integrační testy pustil proti různým implementacím
  úložišť dat (paměť, MySQL databáze, Oracle databáze ...). V aktuální verzi Springu
  se ale v anotaci <a href=\"http://static.springsource.org/spring/docs/3.2.x/javadoc-api/org/springframework/test/context/ContextConfiguration.html\"
  target=\"_blank\">@ContextConfiguration</a> uvádí statický výčet konfiguračních
  XML a to situaci komplikuje.\r\n\r\nŘešení této situace je samozřejmě možné i v
  aktuálním Springu a řešení je nyní dispozici víc. V tomto článku bych chtěl pro
  vás i pro své kolegy popsat to, co se líbí mě ...\r\n\r\n<strong>Varování! </strong>V
  tomto článku nenajdete nic novějšího, než je v dokumentaci Springu - tento článek
  má jediný význam - upozornit na zajímavé novinky ve Spring 3.1+ pro vývojáře, kteří
  (stejně jako já) začínali na starém Springu a některých novinek si v záplavě zpráv
  nestihli všimnout.\r\n\r\n"
wordpress_id: 2561
wordpress_url: http://blog.novoj.net/?p=2561
aliases:
    - /?p=2561
date: '2013-05-11 10:03:00 +0200'
date_gmt: '2013-05-11 09:03:00 +0200'
categories:
- Programování
- Java
- Testování
- Spring Framework
tags: []
comments: []
---
<p><img class="alignleft  wp-image-2564" title="Spring 3.1" src="/binary/2013/05/Logo_Spring_258x151.png" alt="" width="181" height="106" />Po 3 letech dělám větší refaktoring na našem direct mailingovém modulu a jako první jsem se rozhodl povýšit verze knihoven a zrefaktorovat JUnit testy, které jsou tam ještě psané ve stylu JDK 1.4.</p>
<p>V souvislosti s tím jsem samozřejmě přepracoval formu testů z dědičné hierarchie na anotace, které byly představeny poprvé ve <a href="http://static.springsource.org/spring/docs/3.2.x/spring-framework-reference/html/testing.html" target="_blank">Spring 3.X</a> (pokud se nepletu). A tu jsem zjistil, že mám drobný problém - v původní verzi mého kódu jsem využíval dynamické kompozice Springového kontextu k tomu, abych stejné integrační testy pustil proti různým implementacím úložišť dat (paměť, MySQL databáze, Oracle databáze ...). V aktuální verzi Springu se ale v anotaci <a href="http://static.springsource.org/spring/docs/3.2.x/javadoc-api/org/springframework/test/context/ContextConfiguration.html" target="_blank">@ContextConfiguration</a> uvádí statický výčet konfiguračních XML a to situaci komplikuje.</p>
<p>Řešení této situace je samozřejmě možné i v aktuálním Springu a řešení je nyní dispozici víc. V tomto článku bych chtěl pro vás i pro své kolegy popsat to, co se líbí mě ...</p>
<p><strong>Varování! </strong>V tomto článku nenajdete nic novějšího, než je v dokumentaci Springu - tento článek má jediný význam - upozornit na zajímavé novinky ve Spring 3.1+ pro vývojáře, kteří (stejně jako já) začínali na starém Springu a některých novinek si v záplavě zpráv nestihli všimnout.</p>
<p><a id="more"></a><a id="more-2561"></a></p>
<p>K řešení výše uvedeného problému bych mohl sice využít možnost dědičné kompozice aplikačních kontextů, ale musel bych  kopírovat umístění konfiguračních souborů pro jednotlivá úložiště, což je poměrně nepraktické (zvlášť, když je těchto testovacích tříd hodně). Koukněte se sami:</p>
<p><strong>Base test:</strong></p>


``` java
@ContextConfiguration(
  locations = {
    "classpath:/META-INF/lib_mail/spring/test-context.xml"
  }
)
public abstract class AbstractMessageStorageTestCase {
    @Autowired private MessageStorage messageStorage;
    @Test
    public void shouldPersistSimpleMessageBatch() {
       ....
    }
    @Test(expected = RecipientNotDefinedException.class)
    public void shouldFailToPersistMessageBatchWithoutRecipients() {
       ....
    }
}
```

<p><strong>Konkrétní test A:</strong></p>


``` java
@ContextConfiguration(
  locations = {
    "classpath:/META-INF/lib_mail/spring/mysql-datasource.xml",
    "classpath:META-INF/lib_mail/spring/mail-database-dao-config.xml"
  }
)
public class MysqlMessageStorageTest extends AbstractMessageStorageTestCase {
}
```

<p><strong>Konkrétní test B:</strong></p>


``` java
@ContextConfiguration(
  locations = {
    "classpath:/META-INF/lib_mail/spring/memory-dao-config.xml"
  }
)public class MemoryMessageStorageTest extends AbstractMessageStorageTestCase {
}
```

<p>Nehledě na to, že jsem si už nějakou dědičnost v testech zavedl a ta mi celou věc ještě více komplikuje.</p>
<p>Elegantnějším řešením je využít poměrně zajímavou novinku - tzv. <strong>profily.</strong> Ty můžete využít nejen pro testy ale i v produkčním kódu (což je věc, kterou určitě začnu praktikovat). V mém případě jsem si hlavním konfiguračním souboru pro testy udělal následující rozdělení:</p>
<p><strong>Test-config.xml</strong></p>


``` xml
<?xml version="1.0" encoding="utf-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd">
    <!-- sdílené konfigurace pro všechny testy -->
    <import resource="classpath:/META-INF/lib_mail/spring/mail-config.xml"/>
    <import resource="classpath:/META-INF/lib_mail/spring/mail-test-basis.xml"/>
    <!-- specifické nastavení pro profil database -->
    <beans profile="database">
        <!-- tento konfigurační soubor je sdílený pro všechny DB implementace -->
        <import resource="classpath:META-INF/lib_mail/spring/mail-database-dao-config.xml"/>
        <beans profile="mysql">
            <!-- pro mysql ale nahazujeme jiný JDBC datasource -->
            <import resource="datasource-mysql.xml"/>
        </beans>
        <beans profile="oracle">
            <!-- pro oracle ale nahazujeme jiný JDBC datasource -->
            <import resource="datasource-oracle.xml"/>
        </beans>
    </beans>
    <!-- specifické nastavení pro profil memory -->
    <beans profile="memory">
        <import resource="classpath:META-INF/lib_mail/spring/mail-memory-dao-config.xml"/>
    </beans>
</beans>
```

<p>Třída Base test zůstává potom úplně stejná. V jednotlivých testovacích třídách, které mají integračně testovat odlišné implementace úložišť nyní pouze aktivuji profily, které odpovídají konkrétním implementacím:</p>
<p><strong>Konkrétní test A:</strong></p>


``` java
@ActiveProfiles({"database", "mysql"})
public class MysqlMessageStorageTest extends AbstractMessageStorageTestCase {
}
```

<p><strong>Konkrétní test B:</strong></p>


``` java
@ActiveProfiles({"memory"})
public class MemoryMessageStorageTest extends AbstractMessageStorageTestCase {
}
```

<p>Výsledkem je přehledný kód, kde máte všechno pohromadě a přehledně na jednom místě, takže pochopení logiky a použití je přímočaré.</p>
