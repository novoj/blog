---
status: publish
published: true
title: Kafemlejnek.TV 35 – Algolia.com 5 devítek dostupnosti a odezvy do 50ms
aliases:
    - /2018/08/21/kafemlejnek-tv-35-algolia-com-5-devitek-dostupnosti-a-odezvy-do-50ms/
date: '2018-08-21 22:02:31 +0200'
categories:
- Podcast
tags:
- kafemlejnektv
comments: []
---
<p><img data-attachment-id="3405" data-permalink="http://blog.novoj.net/2018/08/09/kafemlejnek-tv-34-algolia-com-fulltextove-vyhledavani-pro-twitch-tv/algolia_logo-svg/" data-orig-file="https://i2.wp.com/blog.novoj.net/binary/2018/08/Algolia_logo.svg_.png?fit=1320%2C1000" data-orig-size="1320,1000" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="Algolia_logo.svg" data-image-description="" data-medium-file="https://i2.wp.com/blog.novoj.net/binary/2018/08/Algolia_logo.svg_.png?fit=300%2C227" data-large-file="https://i2.wp.com/blog.novoj.net/binary/2018/08/Algolia_logo.svg_.png?fit=627%2C475" class="alignleft  wp-image-3405" src="https://i2.wp.com/blog.novoj.net/binary/2018/08/Algolia_logo.svg_.png?resize=165%2C123" alt="" width="165" height="123" srcset="https://i2.wp.com/blog.novoj.net/binary/2018/08/Algolia_logo.svg_.png?zoom=2&amp;resize=165%2C123 330w, https://i2.wp.com/blog.novoj.net/binary/2018/08/Algolia_logo.svg_.png?zoom=3&amp;resize=165%2C123 495w" sizes="(max-width: 165px) 100vw, 165px">V druhém díle s&nbsp;<a href="https://twitter.com/AdamSurak">Adamem Surákem</a>&nbsp;z <a href="https://www.algolia.com/">Algolia.com</a>&nbsp;pokládáme techničtěji zaměřené otázky na jejich vyhledávací stroj. Dozvíte se v něm, že k dosažení 50ms odezev implementovali vyhledávací algoritmy v C++ jako nGinXový modul a provozují jej na vlastním bare metalu a jednom výkonném procesoru. Řeč bude i o SLA a čtyřech (respektive pěti interních) devítkách dostupnosti a jak jich dosahují.</p>
<p><span id="more-3403"></span></p>
<p>Povídat si budeme o tom, že Algolia má implementovanou multi master architekturu a jak se vypořádává s dopady CAP teorému. Tady jsme si trochu zafantazírovali na téma RAFT algoritmu, ale kdo se chce o něm dozvědět více technických faktů, toho radši odkážeme na <a href="https://raft.github.io/">tento článek</a>.</p>
<p>Z celého rozhovoru mi přišlo úžasné, že ačkoliv se Adam převážně stará o infrastrukturu, tak je jeho pozice DevOps zcela oprávněná – o technických detailech toho, jak funguje Algolia, ví skutečně velmi hodně.</p>
<p><strong>Tip:</strong> kdo shlédne video pozorně až do konce, tak si ze závěrečných titulků odnese voucher na vyzkoušení Algolie na 2 měsíce zcela zdarma.</p>
<p>Tento díl jsme natáčeli v prostorách firmy LMC v Lighthouse Towers, v Praze Holešovicích. Za pozvání děkujeme.</p>
<p>Chcete také hostit natáčení Kafemlejnek.TV ve Vaší firmě? <a href="mailto:sleduju@kafemlejnek.tv">Napište nám</a></p>
<p><strong>Kompletní obsah zde:&nbsp;<a href="https://kafemlejnek.tv/dil-35-algolia-com-5-devitek-dostupnosti-a-odezvy-do-50ms/">https://kafemlejnek.tv/dil-35-algolia-com-5-devitek-dostupnosti-a-odezvy-do-50ms/</a></strong></p>
<h4>Obsah</h4>
<ol>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=0m42s">Jak docilujete odezvy do 50ms? 0:42</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=2m10s">Vy si toho děláte hodně sami – engine provozujete na vlastním fyzickém hardware, je to tak? 2:10</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=3m12s">Jak vypadá hardware setup vašeho typického stroje? 3:12</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=3m51s">Proč jste šli do toho starat se o svůj hardware? 3:51</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=6m03s">To, že máte všechna data v RAM, je základní faktor rychlosti? 6:03</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=8m41s">Problematika více procesorových strojů? 8:41</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=9m35s">Do 128GB kompresovaných dat se vejde ohromné množství záznamů, je to tak? 9:35</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=10m20s">Podporujete i shardovaná data nebo se kompletní data musí vždy vejít do 128GB? 10:20</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=14m00s">Algolie je multi-master databáze. Jak řešíte problémy typu split-brain a kde stojíte v CAP teorému? 14:00</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=17m57s">Můžeš nám říct něco víc o algoritmu RAFT? 17:57</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=20m05s">Jo potřeba čas od času udělat kompletní reindexaci dat? 20:05</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=22m22s">Jak řešíte high-availability a jaké máte garance? 22:22</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=26m08s">Jakým způsobem se klientské knihovny adaptují na výpadky serveru v HA setupu? 26:08</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=30m02s">Používáte nějaké externí nástroje pro monitoring provozu infrastruktury? 30:02</a></li>
<li><a href="https://www.youtube.com/watch?v=jEZlsN1DaPU&amp;t=34m37s">Kontribuujete něco většího jako open-source? Co z open-sourcových projektů používáte a co bys doporučil? 34:37</a></li>
</ol>