---
status: publish
published: true
title: Kafemlejnek.TV 37 – Blockchain mimo kryptoměny
aliases:
    - /2018/10/04/kafemlejnek-tv-37-blockchain-mimo-kryptomeny/
date: '2018-10-04 22:02:31 +0200'
categories:
- Podcast
tags:
- kafemlejnektv
comments: []
---
<p><img data-attachment-id="3417" data-permalink="http://blog.novoj.net/2018/10/04/kafemlejnek-tv-37-blockchain-mimo-kryptomeny/p1020635/" data-orig-file="https://i0.wp.com/blog.novoj.net/binary/2018/10/P1020635.jpg?fit=4000%2C2248" data-orig-size="4000,2248" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="P1020635" data-image-description="" data-medium-file="https://i0.wp.com/blog.novoj.net/binary/2018/10/P1020635.jpg?fit=300%2C169" data-large-file="https://i0.wp.com/blog.novoj.net/binary/2018/10/P1020635.jpg?fit=627%2C352" class="alignleft wp-image-3417" src="https://i0.wp.com/blog.novoj.net/binary/2018/10/P1020635.jpg?resize=161%2C93" alt="" width="161" height="93" srcset="https://i0.wp.com/blog.novoj.net/binary/2018/10/P1020635.jpg?zoom=2&amp;resize=161%2C93 322w, https://i0.wp.com/blog.novoj.net/binary/2018/10/P1020635.jpg?zoom=3&amp;resize=161%2C93 483w" sizes="(max-width: 161px) 100vw, 161px">Je jasné, že&nbsp;<a href="https://kafemlejnek.tv/dil-36-uvod-do-technologie-blockchain/">otvírák blockchainu</a> nemohl ani zdaleka pokrýt témata, která jsme chtěli probrat. Pokračovali jsme tedy v rozhovoru s Lukášem Kolískem dál a chtěli jsme po něm nastínit možnosti využití blockchain technologie mimo kryptoměny.</p>
<p><strong>Poznámka:</strong> před díl jsou připojeny medailonky partnerů ne-konference <a href="https://www.jopenspace.cz">jOpenSpace</a>, pokud vás nezajímají a chcete je přeskočit, skočte na 16 minutu 12 vteřinu.</p>
<p><span id="more-3416"></span></p>
<p>Jedním z praktických využití je dokumentace supply chainu, tedy ověřitelné dokumentaci toho odkud se na váš stůl dostaly konkrétní suroviny, a zda nejsou náhodou součástí závadné šarže. U Irů si dokonce <a href="http://www.down-stream.io/">můžete zjistit</a> z čeho, jak a kdy bylo uvařeno to pivo, co právě pijete.</p>
<p>Probrali jsme možnosti využití blockchainu pro identity management a ověřitelná tvrzení (verifiable claims). Zastavili jsme se i u smart kontraktů (skriptování na platformou Ethereum) a z Lukáše vytáhli informaci, že například proof of stake konsenzus algoritmus probíraný v minulém díle je příkladem smart kontraktu. Zjistili jsme také, že smart kontrakty jsou v současné době Turing complete programovací jazyk a&nbsp;dají se v něm psát i <a href="https://cryptozombies.io/">hry</a>.</p>
<p>Jedním z možných use-casů pro využití blockchainu je náhrada notářů. Je to jedna z profesí, která se zdá býti tímto konceptem plně nahraditelná. Krom přízemních problémů, jakým je jejich vliv na zákonodárce, však narážíme i na praktický problém převodu reality do digitálního světa. Kdo a jak zastoupí v digitálním světě smrtku?</p>
<p>Volební systém je další z témat, o kterých se v souvislosti s blockchainem často mluví. Je však reálné a smysluplné klasické papírové lístky digitální formou nahradit? Ukazuje se, že největším problémem je nákup hlasů, které se při digitálním hlasování z domova může projevit mnohonásobně větší silou než nyní.</p>
<p>V závěru jsme se pokusili popasovat i tzv. anonymitou člověka coby účastníka procesu v blockchainu a to tak, že blockchain jako takový anonymitu nezaručuje – ačkoliv v současné době jsou již implementace, které zaručují plnou anonymitu, jsou také implementace, které naopak cílí na plnou transparentnost.</p>
<p>Tímto téma blockchainu opouštíme, abychom se zase za 3 týdny potkali u herního tématu s Honzou Zeleným u české herní hvězdy Mashinky.</p>
<p><strong>Předem děkujeme za sdílení a lajkování našich videí. Stále se nám nedaří na YouTube dosáhnout hranice 1000 odběratelů, která je nutná pro start příjmu z reklam, které by nám pomohly hradit část nákladů na produkci tohoto videocastu.</strong></p>
<p>Pokud by vás zajímala práce v CA Technologies, koukněte na jejich <a href="https://www.ca.com/us/company/careers.html">kariérní stránku</a>, nebo kontaktujte <a href="https://twitter.com/lkolisko">Lukáše</a>.</p>
<p>Chcete také hostit natáčení Kafemlejnek.TV ve Vaší firmě? <a href="mailto:sleduju@kafemlejnek.tv">Napište nám</a></p>
<p><strong>Kompletní obsah:&nbsp;<a href="https://kafemlejnek.tv/dil-37-blockchain-mimo-kryptomeny/">https://kafemlejnek.tv/dil-37-blockchain-mimo-kryptomeny/</a></strong></p>
<ul>
<li style="list-style-type: none;">
<h3>Zajímavé odkazy</h3>
</li>
<li>
<div><a href="https://qz.com/india/1325423/indias-andhra-state-is-using-blockchain-to-build-capital-amaravati/">Blockchain is helping build a new Indian city, but it’s no cure for corruption</a></div>
</li>
<li>
<div><a href="https://xkcd.com/2030/">Using Blockchain for electronic voting</a></div>
</li>
<li>
<div><a href="https://hackernoon.com/difference-between-sidechains-and-state-channels-2f5dfbd10707">Difference Between SideChains and State Channels</a></div>
</li>
<li>
<div><a href="https://youtu.be/IxQUL2ztFi8">Phil Windley on the Sovrin Network</a>&nbsp;<a href="https://www.hyperledger.org/projects/hyperledger-indy" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://www.hyperledger.org/projects/hyperledger-indy&amp;source=gmail&amp;ust=1538673936729000&amp;usg=AFQjCNFHb_-_eoZ6FWF5_RHprA61Gn83mw">(https://www.hyperledger.org/<wbr>projects/hyperledger-indy)</a></div>
</li>
<li>
<div><a href="https://youtu.be/jv3BmGGFP7c">Fabian Vogelsteller – ERC: Identity – Ethereum London</a></div>
</li>
<li>a trochu z opačné strany spektra
<ul>
<li><a href="https://hackernoon.com/ten-years-in-nobody-has-come-up-with-a-use-case-for-blockchain-ee98c180100" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://hackernoon.com/ten-years-in-nobody-has-come-up-with-a-use-case-for-blockchain-ee98c180100&amp;source=gmail&amp;ust=1538673936729000&amp;usg=AFQjCNHkbNvbSa-Y777cso_Si2-sLqIRaA">https://hackernoon.com/ten-<wbr>years-in-nobody-has-come-up-<wbr>with-a-use-case-for-<wbr>blockchain-ee98c180100</a>&nbsp;,</li>
<li><a href="https://medium.com/@kaistinchcombe/decentralized-and-trustless-crypto-paradise-is-actually-a-medieval-hellhole-c1ca122efdec" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://medium.com/@kaistinchcombe/decentralized-and-trustless-crypto-paradise-is-actually-a-medieval-hellhole-c1ca122efdec&amp;source=gmail&amp;ust=1538673936729000&amp;usg=AFQjCNFwxDyD7Obfbju7dYiZL_BR7_XLtw">https://medium.com/@<wbr>kaistinchcombe/decentralized-<wbr>and-trustless-crypto-paradise-<wbr>is-actually-a-medieval-<wbr>hellhole-c1ca122efdec</a></li>
</ul>
</li>
</ul>