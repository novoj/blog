---
status: publish
published: true
title: Kafemlejnek.TV 38 – Od PC Fandu k Mashinkám
aliases:
    - /2018/10/31/kafemlejnek-tv-38-od-pc-fandu-k-mashinkam/
date: '2018-10-31 22:02:31 +0200'
categories:
- Podcast
tags:
- kafemlejnektv
comments: []
---
<p><img data-attachment-id="3426" data-permalink="http://blog.novoj.net/2018/10/31/kafemlejnek-tv-38-od-pc-fandu-k-mashinkam/mashinky_large-320x249/" data-orig-file="https://i0.wp.com/blog.novoj.net/binary/2018/10/mashinky_large-320x249.jpg?fit=640%2C498" data-orig-size="640,498" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="mashinky_large-320×249" data-image-description="" data-medium-file="https://i0.wp.com/blog.novoj.net/binary/2018/10/mashinky_large-320x249.jpg?fit=300%2C233" data-large-file="https://i0.wp.com/blog.novoj.net/binary/2018/10/mashinky_large-320x249.jpg?fit=627%2C488" class="alignleft wp-image-3426" src="https://i0.wp.com/blog.novoj.net/binary/2018/10/mashinky_large-320x249.jpg?resize=204%2C148" alt="" width="204" height="148" srcset="https://i0.wp.com/blog.novoj.net/binary/2018/10/mashinky_large-320x249.jpg?zoom=2&amp;resize=204%2C148 408w, https://i0.wp.com/blog.novoj.net/binary/2018/10/mashinky_large-320x249.jpg?zoom=3&amp;resize=204%2C148 612w" sizes="(max-width: 204px) 100vw, 204px">Na natáčení s Honzou Zeleným – autorem české hry <a href="https://mashinky.com/">Mashinky</a>, jsem se těšil celý rok. Vzhledem k tomu, že letos ještě pracoval na plný úvazek pro Bohemia Interactive a po večerech na Mashinkách, jsme se dohodli, že natáčet budeme, až vznikne nějaký prostor. A ta doba nastala až teď, kdy Honza odešel ze zaměstnání a začal se vývoji hry věnovat naplno.</p>
<p>Tento díl jsme věnovali převážně tomu, jaká byla cesta k Mashinkám. Dozvěděli jsme se, že vyvíjet hry v PC Fandu skutečně nejde. Také jsme se dozvěděli, že skvělé je začínat jako programátor, který debuguje hry ostatních programátorů, protože se na tom hodně naučí.</p>
<p><span id="more-3425"></span></p>
<p>Celý rozhovor s Honzou byl trochu o bourání mýtů. On šel v podstatě proti všem všeobecně uznávaným poučkám – dělal hru jako side project dlouhých 7 let po večerech, místo přepoužití hotových enginů si ten svůj napsal celý od základu sám, a vlastně udělal podle moderních startupových pravidel všechno špatně. A přesto, si myslím, jeho úspěch strčí většinu startupů do kapsy.</p>
<p>V druhé polovině jsme se zaměřili na současný stav a prodeje hry Mashinky a především Honzovy plány do budoucna. Ve hře nás nečeká žádný počítačový protihráč za to je však plánovaný multiplayer, který bude možné hrát i na pokračování (sic!).</p>
<p>Pokud vás Kafemlejnek.TV baví sdílejte nás na sociálních sítích, doporučte kamarádům, lajkujte. Vaše podpora nám pomáhá a dává sílu pokračovat dál!</p>
<p><strong>Kompletní obsah:&nbsp;<a href="https://kafemlejnek.tv/dil-38-od-pc-fandu-k-mashinkam/">https://kafemlejnek.tv/dil-38-od-pc-fandu-k-mashinkam/</a></strong></p>