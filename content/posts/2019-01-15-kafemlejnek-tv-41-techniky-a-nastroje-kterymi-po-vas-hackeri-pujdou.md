---
status: publish
title: Kafemlejnek.TV 41 – Techniky a nástroje, kterými po Vás hackeři půjdou
date: '2019-01-27 14:24:31 +0200'
categories:
- Podcast
tags:
- kafemlejnektv
- hacking
comments: []
---
V druhé části rozhovoru s Pavlem Luptákem z Hacktrophy jsem popustil uzdy fantazii a ptal se Pavla na věci, které mě 
zajímaly v souvislosti s bezpečností na internetu.

Zajímalo mě třebas, jestli se v praxi využívají útoky postranními kanály procesoru (MeltDown, Spectre, TL Bleed). 
Probírali jsme i možné zranitelnosti Intel ME koprocesoru. Dozvěděl jsem se, že řešením řady zranitelností by mohla 
být tzv. kompartmentalizace na úrovni HW i SW.

Od Pavla padla zajímavá myšlenka – paradoxně nejsložitější aplikací, kterou používají běžní uživatelé je webový prohlížeč. 
Proto tam bude vždy plno prostoru pro různé zranitelnosti.

Položil jsem i svou oblíbenou otázku – zda dochází k nějakému výraznějšímu posunu v OWASP žebříčku zranitelností. 
Podle všeho je vidět určitý posun směrem k chytrým klientům – vyplácí se třebas útoky na 
[HTML5 funkcionality](https://www.esecurityplanet.com/trends/article.php/3916381/Top-5-Security-Threats-in-HTML5.htm) typu 
local storage, zpracování videa či SVG obrázků.

Zajímalo mě, jestli a jak hackeři přizpůsobují svoje útoky, podle jazyka, ve kterém je aplikace napsána. 
Což mi Pavel potvrdil – různé platformy mají odlišná slabá místa, kde se vyplatí útočit. Obecně se dá říct, že aplikace 
napsané v Javě či C# jsou bezpečnější než aplikace napsané v PHP. Je to sice zjednodušující tvrzení, ale statisticky 
podpořené. Za tímto tvrzením samozřejmě stojí především úroveň programátorů, kteří jazyk používají a v PHP píše celá 
řada nezkušených vývojářů a to se na statistice zkrátka projevit musí.

Vyptával jsem se i na útoky spadající po oblast sociálního inženýrství. Nejsou součástí standardního, ale u Hacktropy 
je možné si takové útoky přiobjednat. Pavel s nimi má navíc jako člen skupiny Ztohoven dlouholeté zkušenosti. Třeba taková 
[Morální reforma](http://ztohoven.com/mr/index-cs.html) je ukázková aplikace takového sociálního inženýrství.

Tento díl je tedy hlavně o věcech, které nosím v hlavě já. Pokud jste na stejné vlně, věřím, že vás tenhle díl bude bavit.

### Kompletní obsah: https://kafemlejnek.tv/dil-41-techniky-a-nastroje-kterymi-po-vas-hackeri-pujdou/