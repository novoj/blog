---
status: publish
published: true
title: Zbystřete své smysly technickými doplňky
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
excerpt: "<div><img class=\"alignleft  wp-image-2736\" alt=\"HTML 5 Notifications\"
  src=\"/binary/2013/09/html5notifications.png\" width=\"185\"
  height=\"175\" />Nevím jak vám, ale nám se při vývoji často stává, že vývojáři některé
  věci přehlíží a to se nám negativně odráží na produktivitě a kvalitě výstupu. Člověk
  je tvor omylný, ale inteligentní a proto se snaží se vybavit takovými nástroji,
  které jeho nedokonalosti dokáží vyvážit. Na posledním hackathonu kolega <a id=\"\"
  href=\"https://www.facebook.com/michal.kolesnac\" target=\"_blank\" shape=\"rect\">Michal
  Kolesnáč</a> přišel s nápadem a prototypem rozšíření našeho <a href=\"http://blog.novoj.net/2012/09/04/nastroje-pro-vyvoj-web-aplikaci-ve-forrestu/\"
  target=\"_blank\">existujícího doplňku</a> pro Google Chrome, které pomocí <a id=\"\"
  href=\"http://www.html5rocks.com/en/tutorials/notifications/quick/\" target=\"_blank\"
  shape=\"rect\">HTML 5 notifikací</a> upozorní vývojáře na potenciální problémy na
  prohlížené web stránce. Minulý týden jsme řešení dotáhli do konce a myslím, že stojí
  za to, abych se s Vámi o tento nápad podělil.</div>\r\n<div>"
wordpress_id: 2697
wordpress_url: http://blog.novoj.net/?p=2697
aliases:
    - /?p=2697
date: '2013-09-04 16:24:30 +0200'
date_gmt: '2013-09-04 15:24:30 +0200'
categories:
- Programování
- Java
- Softwarové nástroje
- Web
- JavaScript
tags: []
comments:
- id: 152070
  author: frc
  author_email: franc@fg.cz
  author_url: http://
  date: '2013-09-04 19:07:17 +0200'
  date_gmt: '2013-09-04 18:07:17 +0200'
  content: Potvrzuju ze je to silne motivacni. Po prvnim dnu jsem fixnul dlouhodobe
    spatne fungujici cache v uloziati zabezpečení.
- id: 152071
  author: Zuzi
  author_email: zuzi.oko@seznam.cz
  author_url: http://prodej-bytu.hotel-luxus.info/
  date: '2013-09-05 10:42:55 +0200'
  date_gmt: '2013-09-05 09:42:55 +0200'
  content: no já se těšila že to okomentuju páč mě zaujal ten nadpis a vyvolal takové
    emoce že až... Zbystřete své smysly technickými doplňky ale k tomuto nemám až
    zas tak co říct, jen snad to že na to snad budu mít někdy čas a vyzkouším
---
<div><img class="alignleft  wp-image-2736" alt="HTML 5 Notifications" src="/binary/2013/09/html5notifications.png" width="185" height="175" />Nevím jak vám, ale nám se při vývoji často stává, že vývojáři některé věci přehlíží a to se nám negativně odráží na produktivitě a kvalitě výstupu. Člověk je tvor omylný, ale inteligentní a proto se snaží se vybavit takovými nástroji, které jeho nedokonalosti dokáží vyvážit. Na posledním hackathonu kolega <a id="" href="https://www.facebook.com/michal.kolesnac" target="_blank" shape="rect">Michal Kolesnáč</a> přišel s nápadem a prototypem rozšíření našeho <a href="http://blog.novoj.net/2012/09/04/nastroje-pro-vyvoj-web-aplikaci-ve-forrestu/" target="_blank">existujícího doplňku</a> pro Google Chrome, které pomocí <a id="" href="http://www.html5rocks.com/en/tutorials/notifications/quick/" target="_blank" shape="rect">HTML 5 notifikací</a> upozorní vývojáře na potenciální problémy na prohlížené web stránce. Minulý týden jsme řešení dotáhli do konce a myslím, že stojí za to, abych se s Vámi o tento nápad podělil.</div>
<div><a id="more"></a><a id="more-2697"></a></div>
<div>Na úvod se podívejte, jak nám výsledné řešení pomáhá v praxi:</div>
<div></div>
<div>[youtube=https://www.youtube.com/watch?v=dluseSIN3tU]</div>
<h2>
Princip fungování</h2>
<div>Princip je relativně obecný a je určitě přenositelný i do vašeho vývojového ekosystému. O vývoji rozšíření pro Chrome toho už bylo <a id="" href="http://www.zdrojak.cz/clanky/vytvarime-rozsireni-pro-prohlizec-chrome/" target="_blank" shape="rect">napsáno i v češtině</a> docela dost a proto zde nepůjdu do úplných podrobností.</div>
<div></div>
<div>Celý princip je zachycen na následujícím sequence diagramu (btw. vytvořený v <a id="" href="http://www.ckwnc.com/#1002204" target="_blank" shape="rect">http://www.ckwnc.com/</a> ... což je krásná služba pro generování sequence diagramů - jen nedoplňuje popisky k aktorům):</div>
<div></div>
<div><a href="/binary/2013/09/sequence1.png"><img class="aligncenter  wp-image-2702" alt="Sekvenční diagram komunikace" src="/binary/2013/09/sequence1.png" width="580" height="480" /></a></div>
<div>Jednoduše řečeno - každý požadavek na webovou aplikaci prochází servletovým filtrem, který při každém požadavku vygeneruje unikátní token a zapíše do hlaviček odpovědi cookie obsahující URL, na kterém bude v budoucnu odpovídat na požadavky pro zobrazení zpráv spojených s tímto requestem. Po ukončení zpracování HTTP požadavku filtr zanalyzuje aktuální stav aplikace a případně zapíše zprávy pro vývojáře ohledně věcí, kterým by měl věnovat pozornost.</div>
<div></div>
<div>Chrome plugin monitoruje změny cookies a pokud narazí na změnu v cookie se sledovaným názvem, vybere z ní URL, vytvoří XmlHttpRequest a AJAXem se dotáže serveru na seznam zpráv k zobrazení. Požadavek zachytí opět náš servletový filtr, podle unikátního tokenu si ze session vytáhne dříve vygenerovaný seznam zpráv. Nakonec vytvoří JSON zprávu s odpovědí, která obsahuje buď prázdné pole, nebo seznam notifikací s dodatečnými informacemi (např. důležitostí sdělení). JSON je pluginem rozparsován a uživateli jsou prezentovány zprávy jako HTML 5 notifikace. Je důležité si uvědomit, že najednou mohou být zobrazeny pouze 3 notifikace (omezení prohlížeče) a proto je potřeba ty zprávy koncipovat spíše jako odkazy někam dál. V našem případě otvírám po kliknutí na notifikaci <a href="http://blog.novoj.net/2012/09/04/nastroje-pro-vyvoj-web-aplikaci-ve-forrestu/" target="_blank">RamJet Inspektor</a>, kde je k nalezení už konkrétní rozpad problému.</div>
<div></div>
<div>Cílem rozhodně není zahltit vývojáře informacemi - notifikace mají zobrazovat jen informace o důležitých problémech, které vyžadují pozornost a je riziko, že by je vývojář mohl přehlížet. Naopak pokud by je chtěl přehlížet, tak by mu měly notifikace jeho ignoranci alespoň znepříjemnit :).</div>
<div></div>
<div>V našem případě aktuálně monitorujeme tyto problémy:</div>
<div>
<ul>
<li>pomalá odezva stránky (více jak 1 vteřina na vrácení kompletního výstupu)</li>
<li>pomalé SQL dotazy při zpracování požadavku (více jak 200ms na zpracování SQL příkazu)</li>
<li>duplicitní SQL dotazy (špatné použití cachování)</li>
<li>chyby při zpracování stránky (jak při akci, tak i v rámci renderingu) - obvykle by měly být vidět samy od sebe, ale někdy se skryjí ve &lt;script&gt; blocích nebo na stránce chybí komponenta pro výpis chybových hlášení</li>
<li>chyby při aplikaci změn v konfiguraci (refresh Spring kontextů selhal) - jelikož se jede z poslední známé funkční konfigurace, vývojář často problém s reloadem nepostřehne a marně pátrá proč se aplikace nechová tak, jak by podle poslední konfigurace měla</li>
<li>(zvažujeme) použití deprekovaných komponent a funkcí</li>
<li>(plánujeme) zobrazení informace o nelokalizovaných textových popiscích na stránce</li>
</ul>
<p>A také tyto významné informace v životním cyklu aplikace:</p>
<ul>
<li>vypálení události do Spring kontextu (na události navazujeme např. e-mailové notifikace a další <a href="http://en.wikipedia.org/wiki/Observer_pattern" target="_blank">observer</a> akce)</li>
<li>reload konfigurace (tj. aplikování změn v konfiguraci)</li>
</ul>
</div>
<h2>Realizace</h2>
<div>Základem každého plugin je soubor <a id="" href="http://developer.chrome.com/extensions/manifest.html" target="_blank" shape="rect">manifest.json</a>, kam je nutné doplnit seznam oprávnění, které bude naše rozšíření vyžadovat - v našem případě potřebujeme oprávnění: notifications (11), cookies (9) a také komunikaci s aplikačním serverem na stejné doméně po zabezpečeném i nezabezpečeném protokolu (12 a 13). Také si musíme povolit přístup k seznamu ikon, které budeme chtít v notifikacích zobrazovat (15-19) a připojit i JavaScriptový soubor (6), který bude obsahovat logiku, která nám vše oživí.</div>


``` js" highlight="6,9,11
<br />
{<br />
    &quot;name&quot;:&quot;Ramjet Inspector&quot;,<br />
    &quot;version&quot;:&quot;1.8&quot;,<br />
    ... další povinné informace ...<br />
    &quot;background&quot;:{<br />
        &quot;scripts&quot;: [&quot;background.js&quot;]<br />
    },<br />
    &quot;permissions&quot;:[<br />
        &quot;cookies&quot;,<br />
        &quot;tabs&quot;,<br />
        &quot;notifications&quot;,<br />
        &quot;http://*/*&quot;,<br />
        &quot;https://*/*&quot;,<br />
    ],<br />
    &quot;web_accessible_resources&quot;:[<br />
        &quot;skin/INFO.png&quot;,<br />
        &quot;skin/WARNING.png&quot;,<br />
        &quot;skin/ERROR.png&quot;<br />
    ]<br />
}
```

<p>A takhle vypadá obsah JavaScript souboru <strong>background.js</strong>, který obstarává naslouchání na změny v cookies a následné zobrazování notifikací (soubor jsem zkrátil a upravil do srozumitelnější podoby - originál je podstatně delší):</p>


``` js

var openNotifications = 0;
//REGISTRACE LISTENERU NA ZMĚNY V COOKIES
chrome.cookies.onChanged.addListener(function (data) {
    //zajímá nás pouze vytvoření cookie
    if (data.cause == "explicit" && data.removed == false) {
        //s tímto názvem
        if ("RAMJET_COOKIE_NOTIFICATIONS" == data.cookie.name) {
            fetchAndDisplayNotifications(data.cookie);
        }
    }
});
//ZÍSKÁNÍ SEZNAMU NOTIFIKACÍ AJAXEM ZE SERVERU
function fetchAndDisplayNotifications(cookie) {
    var xhr = new XMLHttpRequest();
    var url = getUrlFromCookie(cookie);
    xhr.open("GET", url, true);
    xhr.onload = function () {
        //o výsledek se má zajímat jen když server odpověděl OK
        if (this.status == 200) {
            //v JSONu nám přijde kolekce objektů
            var notifications = JSON.parse(this.responseText);
            for(var i in notifications) {
                showNotification(notifications[i]);
            }
        }
    };
    xhr.send();
}
//ZÍSKÁNÍ URL STRINGU Z COOKIE A ÚPRAVA PROTOKOLU
function getUrlFromCookie(cookie) {
    var url = cookie.value;
    url = url.replace(new RegExp("\"", 'g'), "");
    if("http" != url.substring(0, 4)) {
        var prefix = cookie.secure ? "https://" : "http://";
        url = prefix + cookie.domain + url;
    }
    url = url.replace(new RegExp("\"", 'g'), "");
    return url;
}
//ZOBRAZENÍ NOTIFIKACE
function showNotification(serverNotification) {
    //výchozí callback pouze zruší automatické zmizení notifikace
    var defaultCallback = function () {
        clearTimeout(timeout);
    };
    //notifikaci vytvoříme pouze pokud je místo na obrazovce
    if (openNotifications < 3) {
        var notification = window.webkitNotifications.createNotification(
                'skin/' + serverNotification.severity + '.png',
                'Ramjet inspector',
                serverNotification.text
        );
        notification.show();
        openNotifications++;
        //po 5 vteřinách se notifikace sama zavírá
        var timeout = setTimeout(function () {
            notification.cancel();
            openNotifications--;
        }, 5000);
        //tady řešíme po kliku otevření správného debug okna inspektora
        notification.onclick = defaultCallback
    } else {
        //jinak odložíme zobrazení notifikace o 5 vteřin
        setTimeout(function() { showNotification(notification); }, 5001)
    }
}

```

<p>A takhle vypadá obsah JSON zprávy, která odchází ze servlet filtru:</p>


``` js

[
   {
      "severity": "warning",
      "toolWindow": "debugger",
      "toolWindowTab": "sqlQueries",
      "text": "Při generování stránky se opakují 3 dotazy celkem 5x. Pravděpodobně plýtváš výkonem!"
   },
   {
      "severity": "info",
      "toolWindow": "events",
      "toolWindowTab": null,
      "text": "Při zpracování požadavku došlo k vyvolání události userCreated."
   }
]

```

<h2>A dál?</h2>
<p>Především se potřebujeme přesvědčit, že tento způsob informování vývojáře je použitelný - tedy aby ho naopak zbytečně neobtěžoval. I proto jsem vybíral k notifikaci pouze zásadní situace a problémy v souvislosti s vývojem aplikace (i když si nejsem jist třeba u notifikace pomalé odezvy stránky, která nastává často v souvislosti s debugováním). Pokud se ale osvědčí, budeme chtít udělat podobnou funkcionalitu i pro náš Firefox plugin. Firefox <a href="http://news.softpedia.com/news/Firefox-22-Aurora-Adds-Support-for-HTML5-Desktop-Notifications-344220.shtml" target="_blank">od verze 22</a> totiž již <a href="http://devseo.co.uk/blog/using-the-web-notifications-api" target="_blank">podporuje HTML 5 notifikace</a>. Nicméně vyvíjet pluginy pro Firefox je daleko větší pruda než pro Chrome, takže ten je aktuálně až druhý v pořadí.</p>
<p>Co si o prototypu myslíte? Připadne vám to jako dobrý nápad?</p>
