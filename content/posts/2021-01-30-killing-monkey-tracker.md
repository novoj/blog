---
status: publish
published: true
title: Zkušenosti MonkeyTracker projektu a jeho ukončení
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
date: '2021-01-01 19:20:00 +0200'
date_gmt: '2021-01-01 19:20:00 +0200'
categories:
- Management, Úvahy
tags: [MonkeyTracker]
comments: []
---
MonkeyTracker byl nástroj pro sledování aktivit uživatele na webu. Snímal kliknutí na různá místa na stránce a také místo, 
kam uživatelé doscrollovali myší a kolik obsahu stránky tedy skutečně viděli (měli ochotu zhlédnout). Tyto informace jsou 
užitečné pro UX designery pro vylepšování uživatelského zážitku a při redesignech webů.

Projekt vznikl jako zaměstnanecký projekt v rámci pokusů vyzkoušet si projekt "produktového typu", protože jinak se 
[FG Forrest](https://www.fg.cz) orientuje na zakázkový vývoj. Stáli jsme za ním tři – já a moji kolegové Jakub Kosař a 
Filip Hladík. Já jsem stál za přípravou backendu a provozem, kolegové si rozdělili frontendovou část – tedy JS, který 
monitoroval weby, a následně analytickou konzoli, která vykreslovala teplotní mapy z naměřených výsledků.

V tomto článku bych se s Vámi rád podělil o pár zkušeností, které jsme v průběhu projektu nasbírali, a důvody, proč jej 
vlastně po 6 letech ukončujeme.

Celý článek berte jako moje subjektivní názory coby jednoho z autorů projektu. Nejde o žádná oficiální stanoviska 
[FG Forrest](https://www.fg.cz).

![Úvodní stránka MonkeyTracker.cz][homepage]

[homepage]: /binary/2020/01/www.monkeytracker.cz.png "Úvodní stránka MonkeyTracker.cz"

## Proč službu ukončujeme?

Její provoz jednoduše nedává další ekonomický smysl. Příjem peněz z provozu se dostal pod náklady na údržbu serverů. 
Dalším faktorem byl praktický rozpad původního řešitelského týmu – kolega Filip Hladík odešel z FG (nyní pracuje pro NetSuite) 
a my s Jakubem jsme postupem času přešli k jinému projektu – tím je [Edee.one](https://www.edee.one), který nám bere 100 % našeho času, má mnohem 
větší potenciál a především je ziskový. Konkurence mezitím nespala a třeba český SmartLook nás již daleko překonal 
ve funkcích, které nabízí – aby ne, časová investice do MonkeyTrackeru byla od určité doby prakticky nulová.

## Jaké chyby jsem udělal a jaké si z nich vzal poučení?

Zásadní chybou podle mne bylo, že jsme ve svém středu neměli nikoho obchodně / manažersky orientovaného, který by byl 
zároveň tahounem celého projektu a dokázal nastartovat potřebný růst. V té době jsme byli všichni tři členové týmu 
především vývojáři, a to nebylo správně.

Od začátku jsme MonkeyTracker brali jen jako vedlejší projekt, takže jsme nebyli schopni ani ochotni na něj vsadit vše 
a přepnout na něj veškeré síly. Navíc analytika není ani naše doména, kterou bychom jako autorský tým historicky řešili, 
tj. čerpali jsme know-how a potřeby od UX designerů, se kterými spolupracujeme, a to vůbec nestačilo.

Pokud to mohu srovnat třeba s aktuální situací v [Edee.one](https://www.edee.one), kde e-commercovou část táhne celá řada lidí v čele se skvělým 
obchodníkem s dlouholetou praxí v e-commerce a neméně kvalitním produkťákem, který zná první a poslední okolo e-commerce, 
je ta situace diametrálně odlišná. Bohužel před pěti lety jsme ještě neměli to srovnání, které máme k dispozici dnes.

MonkeyTracker měl také jednu zásadní designovou chybu, která nám ztěžovala nějaký větší růst a vyžadovala i zbytečně 
mnoho komunikace a nepochopení ze strany klientů. Po registraci nového webu bylo nutné zjistit a nastavit takzvaný 
klíčový CSS selektor stránky, který umožnil našemu JS chytnout se nějakého pevného bodu na stránce, který určoval pozici 
prvků u responsivních stránek a umožňoval tak správné měření kliknutí pro různá rozlišení klientských prohlížečů. Valná 
většina responsivních webů totiž řeší třeba 3 skoky pro 3 klíčová rozlišení a vy potřebujete tyto skoky identifikovat 
a na ně zacílit i svá měření. Kliknutí potom byla nahrávána jako relativní X,Y souřadnice vůči levému hornímu rohu DOM 
prvku, který byl identifikovaný CSS selektorem.

Bohužel, jak jsme později zjistili, toto byla zásadní nevýhoda a zároveň s ní nešlo nic jednoduše udělat, protože na tom 
byl celý náš měřící systém postaven. Vyžadovalo to ruční zásah jednoho z nás, a to znamenalo, že Brazilec si nemohl 
jednoduše zaregistrovat doménu a za pár minut koukat, jak se mu vizualizují první kliknutí. Musel si počkat do (našeho) 
rána, až se k tomu jeden z vývojářů dostane. Pro nás to zase znamenalo jistou, byť malou, práci i s klienty, kteří si 
naši službu jen vyzkoušeli a pak zjistili, že jim stačí jen free program, nebo dokonce odešli jinam. Pokud budu někdy 
designovat podobnou službu, už nikdy nesmí být v on-boarding procesu žádný manuální krok a vše musí být úplně 
zautomatizované.

## Co jsem se na projektu naučil a co mě posunulo dál?

Na rovinu přiznávám, že MonkeyTracker byl z valné části o investici volného času autorského týmu. FG dodal především 
HW vybavení, grafiku, texty a další materiální podporu. Ten projekt byl místy zkraje skutečně dost vyčerpávající. Přesto 
jsem velmi rád, že jsme to celé podstoupili, protože jsme se na tom naučili obrovskou spoustu věcí. Minimálně pokud mohu
mluvit za sebe.

V době největšího provozu MonkeyTracker naměřil 35 milionů statistik za měsíc, což odpovídá cca 1.1 mil. dotazů za den. 
Velice brzy nám přestala stačit relační databáze, a tak jsme si vyzkoušeli použití MongoDB jako NoSQL databáze. Nicméně 
nakonec jsme stejně doiterovali k závěru, že klíčové je, jak s daty v aplikaci naložíme. Žádná databáze na světě Vaši 
aplikaci nespasí, pokud jste ji napsali hloupě.

Po třech iteracích jsme dospěli k něčemu, co by se dalo nazývat [kappa architektura](https://www.root.cz/clanky/zpusoby-ulozeni-dat-v-aplikacich-zalozenych-na-mikrosluzbach/#k14). 
Klíčové zjištění pro mne bylo, že není reálně možné v takovém množství teplotní mapy počítat ze zdrojových údajů 
a aplikace musí nutně mít předpřipravené hotové výpočty, které jen uživateli prezentuje, případně vezme několik takových 
výpočtů, provede nad nimi jednoduchou transformaci (třeba součet množin) a ty pak vrátí.

Proto jsme v MongoDB udržovali hotové číselné mapy kliknutí na konkrétních souřadnicích pro jednotlivé sledované stránky 
a konkrétní měsíc, týden a den (den bylo nejnižší možné časové rozlišení). Následně jsme uživateli na stránce při požadavku 
na zobrazení teplotní mapy zobrazili heatmapu generovanou přímo v jeho prohlížeči z předpřipravených dat na serveru, které 
stačilo jedním dotazem vytáhnout z MongoDB. Díky tomu byla naše aplikace opravdu velmi rychlá a umožňovala i některé 
ad-hoc dopočty, které konkurence dlouho neměla (možná doposud nemá). Třeba bylo možné ignorovat v teplotní mapě kousek 
stránky. U bankovních institucí dokáže takové tlačítko pro vstup do online bankovnictví zastínit veškerý ostatní provoz 
na domovské stránce banky. Stejně tak jsme dokázali vysvítit pouze relativní používanost položek v hlavním menu 
a ignorovat naopak provoz zbytku stránky apod.

Pokud si uživatel zobrazil nějaké časové období (např. 2.5 měsíce nazpět), posbírali jsme vypočtené výsledky za největší 
souvislá období (třeba dva celé měsíce, 1 týden, a 4 dny, tj. celkem 7 záznamů z MongoDB), které jsme pak v Javě pomocí 
jednoduché transformace v paměti nad dvoudimenzionálním polem primitivních intů sloučili a poslali na klienta.

MonkeyTracker jsme provozovali ve dvou VPS na českém Wedosu, každá se 2 jádry o frekvenci 1.7 GHz, 8 GB paměti a díky 
této architektuře jsme ani ve špičkách nepotřebovali víc než 20 % výkonu na hardware, který je z dnešního pohledu spíš 
jen k pousmání.

Z tohoto projektu jsem si odnesl, že agregace dat je král a že Java dokáže být neskutečně rychlá bestie, ačkoliv se to 
z určitého pohledu nemusí zdát (třeba když vám [Spring](https://spring.io/) bootuje 200 MB velký WAR přes 2 minuty). V malých aplikacích, 
které tvoří jen malý tým lidí a dává si pozor na výkon od samého počátku, šetří externími dependencemi a vytváří relativně
izolovanou věc s jasným účelem, je výkon Javy prostě parádní. Nevědomky tím obhajuji Microservices architekturu, 
která ovšem přináší celou řadu dalších komplikací, které je nutné vždy pečlivě zvážit. Ne nadarmo se již několik let 
všude možně objevují články lidí, kteří se tímto přístupem spálili.

## Co mě po cestě překvapilo

Osobně mě překvapila ochota lidí strkat si cizí JavaScript kód na vlastní stránky. Možná byla výhoda, že za projektem 
stál [FG Forrest](https://www.fg.cz), který má už vydobytý určitý kredit, ale třeba na homepage webu MonkeyTracker to moc vidět nebylo 
a nemyslím si, že by si klienti MonkeyTracker s FG extra spojovali. MonkeyTracker přitom během prvního půl roku běžel 
na webech řady e-shopů, novin, státních institucí a řady dalších podniků. Z podstaty svého fungování přitom mohl 
odchytávat i privátní data uživatelů a posílat je k nám. Samozřejmě, že jsme to nedělali, ale nikdo z klientů, UX 
designérů a marketérů po tom ani moc nepátral. Podle mě to prostě nikdo moc neřešil a osvěta v této oblasti tu chybí
dodnes.

Doteď mě jímá hrůza, když na weby našich klientů dáváme GTM kontejner, kterým si můžou na svůj web, který jim provozujeme, 
dát libovolný JavaScript odkudkoliv – třeba i z dost pochybných zdrojů. Ochota lidí si na svůj web dát skript kdoví 
odkud je zarážející. Pro nás to byla samozřejmě výhoda – když jsme projekt rozjížděli, neuvědomovali jsme si, že tato 
nedůvěra by mohla projekt zabít ještě před tím, než se narodil.

Dalším překvapením pro mě byl efekt placené recenze nové služby MonkeyTracker na webu známého německého UX analytika. 
Za pár tisíc korun jsme na celé měsíce dostali přísun nových klientů z celého světa, kteří si naši službu chtěli vyzkoušet. 
MonkeyTracker se tak používal i v takových zemích jako je Španělsko, Brazílie nebo Spojené státy. Díky tomu, že článek 
vyšel na jeho blogu, trval přísun těchto leadů i celkem dlouho. Bohužel pro nás jsme z toho nedokázali moc vytěžit.

## Snažíme se nepodcenit ani závěr projektu

MonkeyTracker jsme zavřeli na konci ledna 2021 a rozpustili všechny servery. Vzhledem k tomu, že náš JavaScript je ještě 
na celé řadě cizích webů však neuvolňujeme domény. Mohlo by jednoduše dojít k tomu, že by si naše uvolněné domény koupil 
někdo, komu až tolik na soukromí našich klientů (i těch, co nám nikdy nezaplatili ani korunu) nezáleží a mohl by na ně 
přes nastrčený vlastní JavaScript z koupené domény snadno útočit.

Proto domény přesměrováváme na záložní server, který stejně musíme udržovat z jiných důvodů a na něm budeme monitorovat 
z jakých domén nám ještě chodí požadavky na vrácení MonkeyTracker JavaScriptu. Pokud provoz neustane během půl roku, 
pokusíme se vlastníky těchto domén oslovit, poprosit je o stažení odkazů na nefungující MonkeyTracker, a domény pustit 
teprve ve chvíli, kdy po nějakou dobu už nezaznamenáme žádný požadavek na vrácení měřícího skriptu.