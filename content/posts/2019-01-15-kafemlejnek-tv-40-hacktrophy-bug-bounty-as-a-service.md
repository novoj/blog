---
status: publish
title: Kafemlejnek.TV 40 – Hacktrophy, bug bounty as a service
date: '2019-01-15 21:28:31 +0200'
categories:
- Podcast
tags:
- kafemlejnektv
comments: []
---
Po nějaké době se vracíme k bezpečnostní problematice a tentokrát jsme si pozvali jako hosta Pavla Luptáka ze společnosti 
[Nethemba](https://nethemba.com/cs/">) a [Hacktrophy](https://hacktrophy.com), člena skupiny [Ztohoven](http://www.ztohoven.com/). 
Pavel má 20 let zkušeností z oblasti hackingu, žije nomádským způsobem života a vůbec jsme moc rádi, že se nám jej podařilo 
před kameru dostat.

Společně se zakladatelem společnosti [ESET](https://www.eset.com/cz/) a přáteli ze společnosti [Citadelo](https://citadelo.com/cz/) 
založili před několika lety novou společnost Hacktrophy, která má za cíl zpřístupnit bug bounty programy i menším společnostem. 
Nabízí zprostředkování výzvy k etickému hackingu skupině hackerů, správného nastavení odměn, ověření nahlášených 
zranitelností a vyloučení false/positive hlášení a komunikaci mezi hackerem a zástupcem společnosti, která bug bounty 
program vypisuje.

Toto interview bylo pod mojí taktovkou a zpovídal jsem Pavla na téma Hacktrophy projektu. V druhé části interview, 
který vyjde za 14 dní, jsem se zaměřil na obecnější otázky týkající se bezpečnosti a technik, které k hackingu používají.

Snažil jsem trochu poodkrýt pozadí projektu a kladl jsem otázky, které nás (doufám) ve [Forrestu](https://www.fg.cz) 
jednou dovedou k vypsání vlastního bug bounty programu. Došli jsme totiž ke stejnému závěru, jaký zazní přímo od Pavla 
v rozhovoru - a to, že používání automatických penetračních testů má své limity a po nějaké době už automatizované 
nástroje na odladěné platformě přinášejí minimální užitek. Bug bounty program je logickým dalším krokem na cestě 
k bezpečnější aplikaci.

### Kompletní obsah: https://kafemlejnek.tv/dil-40-hacktrophy