---
status: publish 
published: true 
title: jOpenSpace 2022 
author:
  display_name: Otec Fura 
  login: admin 
  email: novotnaci@gmail.com 
  url: http://blog.novoj.net
author_login: admin 
author_email: novotnaci@gmail.com 
author_url: http://blog.novoj.net
date: '2022-10-10 21:53:00 +0200' 
date_gmt: '2022-10-10 21:53:00 +0200' 
categories:
- Konference
tags: []
comments: []
---

# jOpenSpace 2022 z pohledu organizátora

Trochu s obavami jsme očekávali jaká situace bude v říjnu 2022 a o proto jsme registrace otevřeli až v září. Doba je
skutečně turbulentní a kromě Covidu světem hýbe hned několik dalších krizí najednou. Proto jsme se Zdeňkem Henkem byli
nakonec velmi mile překvapeni, když se registrační číslo přiblížilo třicítce a už na první pohled bylo jasné, díky jménům,
které jsme v registrační listině viděli, že letošní ročník slibuje kvalitní obsah.

![Představení účastníků][predstaveni]

[predstaveni]: /binary/2022/10/predstaveni.jpg "Představení účastníků"

Velmi nám pomáhají naši partneři, kteří zajišťují klíčovou finanční stránku organizace a sami nabídnou zajímavé lidi
ze svých řad. Ze [SentinelOne](https://www.linkedin.com/company/sentinelone/?lipi=urn%3Ali%3Apage%3Ad_flagship3_detail_base%3BL0EqGZs%2FSUKDZgYiCc9RKg%3D%3D)
letos přijeli [Jirka Kadlec](https://www.linkedin.com/in/ACoAAAtEOtIBogvnj6UlGc_rFIU5dHzjnDzXy7Y?lipi=urn%3Ali%3Apage%3Ad_flagship3_detail_base%3BL0EqGZs%2FSUKDZgYiCc9RKg%3D%3D),
[Aleš Rybák](https://www.linkedin.com/in/ACoAAAGWJmEBnV-fHbIY-7FH_7U3cEC4LEPXItE?lipi=urn%3Ali%3Apage%3Ad_flagship3_detail_base%3BL0EqGZs%2FSUKDZgYiCc9RKg%3D%3D) a
[Martin Tošovský](https://www.linkedin.com/in/ACoAAA0ovi0BxLXRb0u9jFhf0hSsrCRFB8d23mc?lipi=urn%3Ali%3Apage%3Ad_flagship3_detail_base%3BL0EqGZs%2FSUKDZgYiCc9RKg%3D%3D),
všichni se zajímavými přednáškami ze světa Javy (jedna o očekávaných a slibných [virtálních vláknech](https://lnkd.in/eSuGBHUV), 
další třeba o [tajích a pastech Stringu](https://lnkd.in/eqvjreB4)). Z [MoroSystems](https://www.linkedin.com/company/morosystems/)
opět přijel [Víťa Plšek](https://www.linkedin.com/in/v%C3%AD%C5%A5a-pl%C5%A1ek-5487a98/) a pokusil se zbourat mýty o
neefektivním E2E testování a zamyslet se nad efektivitou mocků v automatizovaných testech. Z Jihlavského [Commity](https://www.linkedin.com/company/commity-cz/about/)
zase přijel [Milan Lempera](https://www.linkedin.com/in/milanlempera/), který sdílel své pozitivní zkušenost s 
[Data Oriented Programming](https://docs.google.com/presentation/d/1q_D3fKILhEav5mskRqCqocMiMHRtvaUg9r3iz6B8vZA/edit#slide=id.g123d594b28d_0_90)
notně podpořené láskou k závorkám v [Clojure](https://clojure.org/). Z oblasti dev/ops přivezl svou přednášku o 
[toolingu v Go](https://github.com/sikalabs/slu) [Ondřej Šika](https://www.linkedin.com/in/ondrejsika/), zatím co od
[Vaška Pecha](https://www.linkedin.com/in/vaclavpech/) jsme viděli screen IntelliJ Idea verze 1.1 a dozvěděli se, co pro
nás [Jetbrains](https://www.linkedin.com/company/jetbrains/) chystají v oblasti IDE v dalších letech. Já jsem zase přivezl
z kuchyně [FG Forrest](https://www.linkedin.com/company/fgforrest/) na ochutnání první náhled e-commerce databáze [evitaDB](https://evitadb.io),
kterou se chystáme příští rok vydat.

Naši vyzkoušení kameramané, na které jsme byli zvyklí z minulých let se rozprchli do světa a zkouší kariéru v Holandsku,
a proto jsme byli nuceni letos najít náhradu. Zdá se, že jsme hned napoprvé trefili správně a s techniky z [týmu p. Maška](https://masek.eu/)
jsme si sedli na výbornou. Dobře dopadl i livestream a věřím, že záznamy a střižená videa z ne-konference dopadnou na
jedničku a brzy je budeme moci zveřejnit. Stejně tak můžeme doporučit i služby hotelu [Antoň](https://www.hotel-anton.cz/),
který nám celou akci pomohl uspořádat za velice vstřícných podmínek (a to to s námi skutečně nemá jednoduché).

V sobotu v sedm ráno se pravidelně pár statečných vydá na krátký výběh do přilehlých lesů. Letos v poněkud omezenějším
počtu - pravidelné účastníky v podobě mé maličkosti a [Pavla Jetenského](https://www.linkedin.com/in/paveljetensky/), 
letos doplnil i [Michal Těhník](https://www.linkedin.com/in/mictech/) a běh jsme strávili v příjemné diskusi o rozvoji
[Productboardu](https://www.linkedin.com/company/productboard/) a technologických posunech, které u nich probíhají.

![Ranní výběh][vybeh]

[vybeh]: /binary/2022/10/vybeh.jpg "Ranní výběh"

V nabitém přednáškovém programu si každý vybral to své - letos to byl skutečně pestrý mix od 
[rozboru Shorova algoritmu v kvantových počítačích](https://docs.google.com/presentation/d/1qG1tuSuJvNjVHJ57_nUSPp4PBhHVMD-2HUvSlHorgJg/edit#slide=id.p),
přes [Interplanetary File System](https://docs.google.com/presentation/d/1PIOxEp4uRPeNIFV30gdmsPcWEyVuyr6fUX3aQnBSazQ/edit#slide=id.p),
k tipům na [užitečné](https://docs.google.com/presentation/d/1FsKxsU8FKFCKsc0qoxJZJljkz6ckERmCcVIhoM68Qk0/edit#slide=id.p)
[nástroje](https://github.com/sikalabs/slu) či rozboru [informačního vybavení vojáků na Ukrajině](https://docs.google.com/presentation/d/1Vmcn2oBRXBc68bs_f4celEFGJPSfhT_g2782UuCjDgc/edit#slide=id.p).
Nechyběla ani zajímavá přednáška o [hře šachy](https://dynatrace-my.sharepoint.com/:p:/p/lubomir_petera/ETny2A8aSVZDuTjfpvW-DtgBpSAh8yVdwZGA3OrB4_Fvew?rtime=BEPNqP2q2kg),
která vyústila ve večerní bitvu.

![Večerní šachy][sachy]

[sachy]: /binary/2022/10/sachy.jpg "Večerní šachy"

Sobotní program jsme zakončili až okolo druhé ranní, diskusí nad mladickými úspěchy ve hrách Golden Axe, či Prince 
of Persia a vyměnili si pár tipů i na hry současné jako jsou XCOM2 či DRG. Zkrátka nudit se prostě nebylo kdy.

Nedělní program je obvykle už trochu volnější a všichni jsou po sobotě tak trochu unavení. Nicméně i tak jsme ráno 
společně s [Pavlem Jetenským](https://www.linkedin.com/in/paveljetensky/) prošli jeho inspirativní tipy při práci v
[IntelliJ Idea](https://www.jetbrains.com/idea/) - třeba tip na [hledání v předchozích výsledcích](https://intellij-support.jetbrains.com/hc/en-us/community/posts/207060255-Search-within-search-results-)
nebo kombinace [sloupcového výběru a hledání](https://www.vojtechruzicka.com/intellij-idea-tips-tricks-multiple-cursors/#select-all-occurrences)
byla překvapením i pro ostřílené programátory. Já jsem v dalším workshopu sbíral první feedback na GraphQL API pro
[evitaDB](https://evitadb.io) a zkoušel vysvětlit principy, které za naší databází stojí. Zdá se, že se mi podařilo
vzbudit pozitivní první dojem a také jsem na základě mnoha dotazů zjistil, co vývojáře zajímá především a co považují
u databáze za klíčové. Pauzu před obědem pak využil [Víťa Plšek](https://www.linkedin.com/in/v%C3%AD%C5%A5a-pl%C5%A1ek-5487a98/),
který se podělil o zkušenosti s cestováním v auto karavanu po Evropě a praktických radách, co od takového výletu může
člověk s rodinou čekat.

![Fotografie účastníků][spolecna]

[spolecna]: /binary/2022/10/spolecna.jpg "Fotografie účastníků"

Před obědem nás čekala už jen poslední společná fotka a vyhodnocení nejoblíbenějších přednášek celého víkendu. Ačkoliv
jsem sám žádnou cenu nevyhrál, jsem za čtvrtou, bramborovou příčku rád. Díky všem za parádní a inspirativní víkend, ze
kterého budu zase ještě dlouho čerpat energii.