---
status: publish
published: true
title: Combining custom annotations for securing methods with Spring Security
author:
  display_name: Otec Fura
  login: admin
  email: novotnaci@gmail.com
  url: http://blog.novoj.net
author_login: admin
author_email: novotnaci@gmail.com
author_url: http://blog.novoj.net
excerpt: "<img class=\"alignleft  wp-image-1858\" title=\"spring_security\" src=\"/binary/2012/03/spring_security_extjs_login-e1332024054704.png\"
  alt=\"\" width=\"149\" height=\"58\" /> <a href=\"http://static.springsource.org/spring-security/site/\"
  target=\"_blank\">Spring security</a> is really powerful library in its current
  version and I like it much. You can secure your application on method level several
  years now (this feature was introduced by <a href=\"http://www.nofluffjuststuff.com/blog/craig_walls/2008/04/method_level_security_in_spring_security_2_0\"
  target=\"_blank\">Spring Security 2 in 4/2008</a>) but we've upgraded from old Acegi
  Security only recently. When using method access control in larger scale I started
  to think about security rules encapsulation into standalone annotation definitions.
  It's something you can live without but in my opinion it could help readibility
  and maintainability of the code. Let's present some options we have now ...\r\n\r\n"
wordpress_id: 1857
wordpress_url: http://blog.novoj.net/?p=1857
aliases:
    - /?p=1857
date: '2012-03-27 21:42:36 +0200'
date_gmt: '2012-03-27 20:42:36 +0200'
categories:
- Programování
- Java
- Spring Framework
- English
tags:
- security
comments:
- id: 64798
  author: Victor Dario Martinez
  author_email: dariovmartine@gmail.com
  author_url: ''
  date: '2012-03-29 18:47:06 +0200'
  date_gmt: '2012-03-29 17:47:06 +0200'
  content: I just voted in jira. good luck!
- id: 65138
  author: Zubbate
  author_email: glubokaya@gmail.com
  author_url: ''
  date: '2012-04-05 16:39:13 +0200'
  date_gmt: '2012-04-05 15:39:13 +0200'
  content: This approach is almost exactly what I need. But I tried to configure secutiry
    context the way you wrote but got no security handling. I see that my methods
    are called w/o interception. Could you please help me out what can be wrong?
- id: 65164
  author: Otec Fura
  author_email: novotnaci@gmail.com
  author_url: http://blog.novoj.net
  date: '2012-04-06 07:02:30 +0200'
  date_gmt: '2012-04-06 06:02:30 +0200'
  content: I'd like to, but it's hard to guess without any data. Would it be possible
    for you to send me your project / part of the project with some failing tests?
    I might find out what's wrong then.
- id: 73769
  author: "&raquo; How do YOU test access control of your application? Myšlenky dne
    otce Fura"
  author_email: ''
  author_url: http://blog.novoj.net/2012/06/14/how-do-you-test-access-control-of-your-application/
  date: '2012-06-14 23:11:10 +0200'
  date_gmt: '2012-06-14 22:11:10 +0200'
  content: "[...] If you wonder what @Allowed* annotations mean you might want to
    read another my article Custom annotations for Spring Security &#8211; but in
    short you can imagine them as multiple @PreAuthorize annotations with some SpEL
    in [...]"
- id: 148464
  author: "&raquo; Šestý rok Myšlenek dne otce Fura Myšlenky dne otce Fura"
  author_email: ''
  author_url: http://blog.novoj.net/2013/01/01/sesty-rok-myslenek-dne-otce-fura/
  date: '2013-01-01 00:01:19 +0100'
  date_gmt: '2012-12-31 23:01:19 +0100'
  content: "[...] vpřed především díky poměrně masivnímu nasazení Spring Security,
    o kterém jsem psal i několik [...]"
- id: 152059
  author: Spring Security - Custom annotation for securing methods | Java CodeBook
  author_email: ''
  author_url: http://javacodebook.com/2013/07/05/spring-security-custom-annotation-for-securing-methods/
  date: '2013-08-04 19:30:48 +0200'
  date_gmt: '2013-08-04 18:30:48 +0200'
  content: "[...] http://blog.novoj.net/2012/03/27/combining-custom-annotations-for-securing-methods-with-spring-secur...
    Page Visitors: 4 [...]"
- id: 156241
  author: NoSuchBeanDefinitionException, but bean is defined | FYTRO SPORTS
  author_email: ''
  author_url: http://fytro.info/nosuchbeandefinitionexception-but-bean-is-defined/
  date: '2015-06-16 16:06:22 +0200'
  date_gmt: '2015-06-16 15:06:22 +0200'
  content: "[&#8230;] I am attempting to get the configurations into Spring Boot using
    Java based config instead of XML config from this blog post: http://blog.novoj.net/2012/03/27/combining-custom-annotations-for-securing-methods-with-spring-secur&#8230;
    [&#8230;]"
- id: 157545
  author: "&raquo; Reportáž z GeeCON Praha 2015 Myšlenky dne otce Fura"
  author_email: ''
  author_url: http://blog.novoj.net/2015/10/26/reportaz-z-geecon-praha-2015/
  date: '2015-10-26 10:35:34 +0100'
  date_gmt: '2015-10-26 09:35:34 +0100'
  content: "[&#8230;] kompozice anotací (je velká škoda, že podobnou věc nelze udělat
    i ve Spring Security &#8211; což sice lze obejít, ale dost pracně). Zdůrazňoval
    respektování použitých generik v třídách autowirovaných [&#8230;]"
---
<p><img class="alignleft  wp-image-1858" title="spring_security" src="/binary/2012/03/spring_security_extjs_login-e1332024054704.png" alt="" width="149" height="58" /> <a href="http://static.springsource.org/spring-security/site/" target="_blank">Spring security</a> is really powerful library in its current version and I like it much. You can secure your application on method level several years now (this feature was introduced by <a href="http://www.nofluffjuststuff.com/blog/craig_walls/2008/04/method_level_security_in_spring_security_2_0" target="_blank">Spring Security 2 in 4/2008</a>) but we've upgraded from old Acegi Security only recently. When using method access control in larger scale I started to think about security rules encapsulation into standalone annotation definitions. It's something you can live without but in my opinion it could help readibility and maintainability of the code. Let's present some options we have now ...</p>
<p><a id="more"></a><a id="more-1857"></a></p>
<h2>Problem decomposition - how to organize our rules?</h2>
<h3>Have no system at all</h3>
<p>This is usually the first approach one can take after reading documentation of Spring Security framework and start to use it in own code. All rules are represented by Strings of <a href="http://static.springsource.org/spring/docs/3.0.5.RELEASE/reference/expressions.html" target="_blank">SpEL</a> as follows:</p>


``` java

@PreAuthorize("principal.userObject.administrator")
public void approveOrganization(Organization organization) {
   //... content ...
}
@PreAuthorize("principal.userObject.isOwnerOf(#organization.id)
               or principal.userObject.administrator")
public void updateOrganization(Organization organization) {
   //... content ...
}
@PreAuthorize("(branch.isPartOf(organization) and principal.userObject.isManagerOf(#branch.id))
                or principal.userObject.isOwnerOf(#organization.id)
                or principal.userObject.administrator")
public void updateOrganizationBranch(Organization organization, Branch branch) {
   //... content ...
}
//and more ... example was shortened

```

<p>After a while you might notice that certain rules keep repeating. You can see these rules repeat in our example class:</p>


``` java

principal.userObject.administrator
// means administrator with super rights is logged in
principal.userObject.isOwnerOf(#organization.id)
// means user that is owner of particular organization
//(has super rights related to his organization) is logged in
branch.isPartOf(organization) and principal.userObject.isManagerOf(#branch.id)
// means user that is manager of particular branch
// (has rights for operations connected with his branch) is logged in

```

<p>As we all know Strings are evil - you can't refactor them safely and they tend to break your code much more often. So after a while you start to ...</p>
<h3>Extract rules into constants</h3>
<p>So the code starts to look like this:</p>


``` java

private static final String ALLOWED_FOR_ADMINISTRATOR = "principal.userObject.administrator";
private static final String ALLOWED_FOR_OWNER = "principal.userObject.isOwnerOf(#organization.id)";
private static final String ALLOWED_FOR_BRANCH_MANAGER = "(branch.isPartOf(organization) and principal.userObject.isManagerOf(#branch.id))";
@PreAuthorize(ALLOWED_FOR_ADMINISTRATOR)
public void approveOrganization(Organization organization) {
   //... content ...
}
@PreAuthorize(ALLOWED_FOR_OWNER + " or " + ALLOWED_FOR_ADMINISTRATOR)
public void updateOrganization(Organization organization) {
   //... content ...
}
@PreAuthorize(ALLOWED_FOR_OWNER + " or " +
              ALLOWED_FOR_ADMINISTRATOR + " or " +
			  ALLOWED_FOR_BRANCH_MANAGER)
public void updateOrganizationBranch(Organization organization, Branch branch) {
   //... content ...
}
//and more

```

<p>Or you could create central shared repository of security rules to keep them in single place to have some control over them:</p>


``` java

public abstract class SecurityRules {
   public static final String ALLOWED_FOR_ADMINISTRATOR = "principal.userObject.administrator";
   public static final String ALLOWED_FOR_OWNER = "principal.userObject.isOwnerOf(#organization.id)";
   public static final String ALLOWED_FOR_BRANCH_MANAGER = "(branch.isPartOf(organization) and
                                                    principal.userObject.isManagerOf(#branch.id))";
}

```

<p>Bad things happen when you'd like to share some rules across different libraries. For example we have a legacy system using its own security solution for accessing backend administration. SuperAdministrators are authenticated by this legacy system and have supervisor rights in most of our simple applications - so when such admin is authenticated all Spring Security rules should be overriden and all methods should become accessible for such user. But how to share this common rule across different customer installations? I can make up only single solution possible with standard Spring Security behaviour:</p>


``` java

@PreAuthorize(SharedSecurityRules.ALLOWED_FOR_SUPERVISOR + " or " +
              SecurityRules.ALLOWED_FOR_ADMINISTRATOR)
public void approveOrganization(Organization organization) {
   ... content ...
}
@PreAuthorize(SharedSecurityRules.ALLOWED_FOR_SUPERVISOR + " or " +
              SecurityRules.ALLOWED_FOR_OWNER + " or " +
              SecurityRules.ALLOWED_FOR_ADMINISTRATOR)
public void updateOrganization(Organization organization) {
   ... content ...
}
@PreAuthorize(SharedSecurityRules.ALLOWED_FOR_SUPERVISOR + " or " +
              SecurityRules.ALLOWED_FOR_OWNER + " or " +
              SecurityRules.ALLOWED_FOR_ADMINISTRATOR + " or " +
              SecurityRules.ALLOWED_FOR_BRANCH_MANAGER)
public void updateOrganizationBranch(Organization organization, Branch branch) {
   ... content ...
}

```

<p>And I don't see this kind of notation much more readable than our first attempt with raw SpEL strings. What could be done with this?</p>
<h3>Extract rules into custom annotations</h3>
<p>You can extract your rules into custom annotations and Spring would find them. If you look at AnnotationUtils class you would realize that Spring tries to find annotation not only on method itself but also on annotations of this method. So you can have following custom annotation:</p>


``` java

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize(AllowedForOrganizationOwner.IS_ORGANIZATION_OWNER)
public @interface AllowedForOrganizationOwner {
    String IS_ORGANIZATION_OWNER = "principal.userObject.isOwnerOf(#organization.id)";
}

```

<p><strong>But there is ONE BIG "BUT"</strong></p>
<p>You can't mix multiple annotations together - Spring will find only the first applicable annotation and uses this one. So we get no further. This solution seems unusable at the first glance. </p>
<p>Wouldn't it be great if we could define something like this?:</p>


``` java

@AllowedForAdministrator
public class MyManager {
   public void approveOrganization(Organization organization) {
      ... content ...
   }
   @AllowedForOrganizationOwner
   public void updateOrganization(Organization organization) {
      ... content ...
   }
   @AllowedForOrganizationOwner
   @AllowedForBranchManager
   public void updateOrganizationBranch(Organization organization, Branch branch) {
      ... content ...
   }
}

```

<p>Do you like it? Does it seem more readable for you?</p>
<p>If so read on - there is a way how to achieve this ...</p>
<h2>Compositing security annotations</h2>
<p>Let's define following composition system:</p>
<ul>
<li>if there are multiple security annotation placed on the same place (ie. method or class) they represent disjunction (ie. logical OR) ... but we might have a way how to change composition behavior to logical AND if necessary</li>
<li>rules placed on class level combine with method rules always by logical OR - if class defined rules say access allowed they should override possible access denied vote from the rules placed on method</li>
</ul>
<p>Let's have some examples:<br />


``` java

@AllowedForAdministrator
public class MyManager {
   public void approveOrganization(Organization organization) {
      //has no method annotation - class annotation will be used instead
   }
   @AllowedForOrganizationOwner
   public void updateOrganization(Organization organization) {
      //method and class annotations are combined with OR relation
   }
   @AllowedForOrganizationOwner
   @AllowedForBranchManager
   public void updateOrganizationBranch(Organization organization, Branch branch) {
      //method annotations are combined by default with OR relation
      //result it then combined with class annotation with OR relation
      //results in administrator or organization owner or branch manager
   }
   @AllowedForAnyone
   public Organization getOrganizationById(Integer id) {
      //we want to let this method to by called by anyone but if we would
      //not annotate it at all class annotation will deny execution to all
      //but the administrator - so we need to add following annotation:
      //AllowedForAnyone => @PreAuthorize("true")
   }
   @RulesRelation(BooleanOperation.AND)
   @IsAuthenticatedFully
   @AllowedForOrganizationOwner
   public void removeOrganizationById(Integer id) {
      //when we want to change relationship to require all rules to be satisfied
      //we have to use special annotation @RulesRelation changing default relationship among rules
      //execution of this method requires user to be manually logged in in current session
      //and to represent an organization owner, of course administrator could call this
      //method too because class annotations are always added with OR relation
   }
}

```

<h2>Tweaking the Spring Security</h2>
<p>If you integrate Spring Security into your project you'd probably use the shortcut way represented by <a href="http://static.springsource.org/spring-security/site/docs/3.0.x/reference/ns-config.html#ns-method-security" target="_blank">security namespace</a>. Unfortunatelly interface that needs to be overriden is was not exposed by this namespace and is somehow supported by the most recent version 3.1 somehow (see <a href="https://jira.springsource.org/browse/SEC-1383" target="_blank">issue SEC-1383</a>). But I wasn't able to make it running and was forced to initialize method security in the old way as a set of aspects:</p>


``` xml

<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-3.0.xsd">
    <aop:config proxy-target-class="true">
        <aop:advisor advice-ref="experimentalMethodSecurityInterceptor"
                     pointcut="execution(@(@org.springframework.security.access.prepost.PreAuthorize *) * *.* (..))"/>
        <aop:advisor advice-ref="experimentalMethodSecurityInterceptor"
                     pointcut="execution(@(@org.springframework.security.access.prepost.PreFilter *) * *.* (..))"/>
        <aop:advisor advice-ref="experimentalMethodSecurityInterceptor"
                     pointcut="execution(@(@org.springframework.security.access.prepost.PostAuthorize *) * *.* (..))"/>
        <aop:advisor advice-ref="experimentalMethodSecurityInterceptor"
                     pointcut="execution(@(@org.springframework.security.access.prepost.PostFilter *) * *.* (..))"/>
    </aop:config>
    <!-- Configure custom security interceptor -->
    <bean id="experimentalMethodSecurityInterceptor"
          class="org.springframework.security.access.intercept.aopalliance.MethodSecurityInterceptor">
        <property name="securityMetadataSource">
            <bean class="cz.novoj.spring.security.aop.ExperimentalPrePostAnnotationSecurityMetadataSource">
                <constructor-arg>
                    <bean class="org.springframework.security.access.expression.method.ExpressionBasedAnnotationAttributeFactory">
                        <constructor-arg ref="expressionHandler"/>
                    </bean>
                </constructor-arg>
            </bean>
        </property>
        <property name="authenticationManager" ref="authenticationManager"/>
        <property name="validateConfigAttributes" value="false"/>
        <property name="accessDecisionManager">
            <bean class="org.springframework.security.access.vote.AffirmativeBased">
                <constructor-arg>
                    <list>
                        <bean class="org.springframework.security.access.prepost.PreInvocationAuthorizationAdviceVoter">
                            <constructor-arg>
                                <bean class="org.springframework.security.access.expression.method.ExpressionBasedPreInvocationAdvice">
                                    <property name="expressionHandler" ref="expressionHandler"/>
                                </bean>
                            </constructor-arg>
                        </bean>
                    </list>
                </constructor-arg>
            </bean>
        </property>
    </bean>
    <bean id="expressionHandler"
          class="org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler"/>
</beans>

```

<p><em><strong>Note:</strong>you need to declare AspectJ pointcuts as follows if you want to find not only methods annotated directly with certain annotation type but also methods annotated with annotations annotated with such annotation type:</em></p>


``` xml

execution(@(@org.springframework.security.access.prepost.PreAuthorize *) * *.* (..))

```

<p>As you can see - all classes except one are taken from Spring codebase. The single exception is class <b>ExperimentalPrePostAnnotationSecurityMetadataSource</b> that is meant to be used instead of original <b>PrePostAnnotationSecurityMetadataSource</b> that looks up for annotations of @PreAuthorize, @PreFilter, @PostAuthorize and @PostFilter. My experimental version finds all annotations that represents above mentioned security annotations or are annotated with them. It retrieves all SPeL security rules that these annotations contain and combine them into a single one in context initialization time. Principles of the rules combinations are stated in the beginning of the previous chapter.</p>
<p>Source code of all used extension classes are available in form of GIST (implementation details are not so important for this article as the main idea is): <a href="https://gist.github.com/2081353">https://gist.github.com/2081353</a></p>
<p>If you find this idea useful - please vote or comment Spring Security issue I've created for this idea: <a href="https://jira.springsource.org/browse/SEC-1945" target="_blank"><b>Spring Security issue documenting this idea</b></a></p>
